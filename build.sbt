import Dependencies._

ThisBuild / organization := "com.example"
ThisBuild / scalaVersion := "3.4.1"
ThisBuild / version      := "0.1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(
    name := "AoC-2018",
    maintainer := "Lorenz Pahl <l.pahl@outlook.com>",
    licenses += "Apache License, Version 2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0"),

    packageSummary := "Advent of Code 2018 Solutions",
    packageDescription := "Solutions to the Advent of Code 2018 puzzles.",

    semanticdbEnabled := true,
    scalacOptions ++= Seq(
      "-encoding", "UTF-8",
      "-unchecked",
      "-deprecation"
    ),

    libraryDependencies ++= Seq(scalaTest % Test)
  )
