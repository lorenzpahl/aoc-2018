/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import java.io.PrintStream
import java.time.Duration

import aoc.IOUtils.readAllLines

import scala.jdk.StreamConverters.*
import scala.language.implicitConversions

/** --- Day 18: Settlers of The North Pole - Solution ---
  *
  *   - Part I: 560091
  *   - Part II: 202301
  */
object Day18:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_18_input.txt")
    val lumberCollectionArea = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val (areas, repeatingAreas) = simulateLandscapeChange(lumberCollectionArea)
    val calculateResourceValueAfter = resourceValueAfter(areas, repeatingAreas)

    val resourceValuePartI = calculateResourceValueAfter(Duration.ofMinutes(10))
    println(s"Part I: The total resource value after 10 minutes is ${resourceValuePartI}.")

    val resourceValuePartII = calculateResourceValueAfter(Duration.ofMinutes(1_000_000_000))
    println(s"Part II: The total resource value after 1,000,000,000 minutes is ${resourceValuePartII}.")

  def resourceValueAfter(
      areas: LazyList[LumberCollectionArea],
      repeatingAreas: LazyList[LumberCollectionArea]
  )(duration: Duration): Int =
    val minutes = duration.toMinutes.toInt
    val predictedArea =
      if areas.sizeIs > minutes then areas(minutes)
      else
        val idx = Math.floorMod(minutes - areas.size, repeatingAreas.size)
        repeatingAreas(idx)

    val acreCountByType = predictedArea.flatten.groupMapReduce(acre => acre)(_ => 1)(_ + _).withDefaultValue(0)
    acreCountByType(Acre.Trees) * acreCountByType(Acre.Lumberyard)

  def simulateLandscapeChange(lumberCollectionArea: LumberCollectionArea)
      : (LazyList[LumberCollectionArea], LazyList[LumberCollectionArea]) =
    val initialCache = Map.empty[LumberCollectionArea, Option[LumberCollectionArea]]
    val entries = LazyList.iterate((lumberCollectionArea, (Option.empty[LumberCollectionArea], initialCache))) {
      case (currArea, (_, cache)) =>
        val computeNextStateAt = nextStateAt(currArea)

        val nextArea = currArea.view.zipWithIndex.map { (lane, y) =>
          lane.view.zipWithIndex.map { (acre, x) => computeNextStateAt((x, y), acre) }.toIndexedSeq
        }.toIndexedSeq

        cache.get(nextArea) match
          case None =>
            val updatedCache = cache.updated(nextArea, None).updatedWith(currArea) {
              case Some(None)         => Some(Some(nextArea))
              case currSucc @ Some(_) => currSucc
              case _                  => None
            }

            (nextArea, (None, updatedCache))
          case _ => (nextArea, (Some(nextArea), cache))
    }

    val (areas, repeatingEntries) = takeWhile(entries) {
      case (_, (maybeRepeatingArea, _)) => maybeRepeatingArea.isEmpty
    }.partitionMap {
      case (area, (None, _))        => Left(area)
      case (_, (Some(area), cache)) => Right(area -> cache)
    }

    val repeatingAreas = LazyList.iterate(repeatingEntries.headOption) {
      case Some((area, cache)) => cache.get(area) match {
          case Some(Some(succ)) => Some(succ -> cache)
          case _                => None
        }
      case _ => None
    }.takeWhile(_.isDefined)
      .collect { case Some((area, _)) => area }

    areas -> repeatingAreas

  def nextStateAt(lumberCollectionArea: LumberCollectionArea)(pos: (Int, Int), acre: Acre): Acre =
    val (x, y) = pos
    val xs = (x - 1) to (x + 1)
    val ys = (y - 1) to (y + 1)
    val neighbours = xs.view
      .flatMap(x => ys.map(y => x -> y))
      .filter(neighbourPos => neighbourPos != pos)
      .filter { (neighbourX, neighbourY) =>
        ((neighbourY >= 0) && (neighbourY < lumberCollectionArea.size)) &&
        ((neighbourX >= 0) && (neighbourX < lumberCollectionArea(neighbourY).size))
      }
      .map { (neighbourX, neighbourY) => lumberCollectionArea(neighbourY)(neighbourX) }

    acre match
      case Acre.OpenGround =>
        val trees = neighbours.collect { case Acre.Trees => Acre.Trees }

        if trees.sizeIs >= 3 then Acre.Trees else Acre.OpenGround
      case Acre.Trees =>
        val lumberyards = neighbours.collect { case Acre.Lumberyard => Acre.Lumberyard }

        if lumberyards.sizeIs >= 3 then Acre.Lumberyard else Acre.Trees
      case Acre.Lumberyard =>
        val hasLumberyard = neighbours.exists {
          case Acre.Lumberyard => true
          case _               => false
        }

        lazy val hasTrees = neighbours.exists {
          case Acre.Trees => true
          case _          => false
        }

        if hasLumberyard && hasTrees then Acre.Lumberyard else Acre.OpenGround

  def printArea(lumberCollectionArea: LumberCollectionArea)(writer: PrintStream = Console.out): Unit =
    lumberCollectionArea.foreach { lane =>
      lane.foreach {
        case Acre.OpenGround => writer.print("\u2591")
        case Acre.Trees      => writer.print("|")
        case Acre.Lumberyard => writer.print("\u2593")
      }
      writer.println()
    }

  def parseData(lines: Iterator[String]): LumberCollectionArea =
    lines.map { line =>
      line.chars().toScala(IndexedSeq).map {
        case '.' => Acre.OpenGround
        case '|' => Acre.Trees
        case '#' => Acre.Lumberyard
        case _   => throw new IllegalArgumentException()
      }
    }.toIndexedSeq

  def takeWhile[A](xs: LazyList[A])(p: A => Boolean): LazyList[A] =
    var done = false
    val pp: A => Boolean = a =>
      !done && {
        if !p(a) then done = true
        true
      }
    xs.takeWhile(pp)

  type LumberCollectionArea = IndexedSeq[IndexedSeq[Acre]]

  enum Acre:
    case OpenGround
    case Trees
    case Lumberyard
