/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import scala.util.CommandLineParser.FromString

object PuzzleRunner:
  enum Puzzle(f: => Unit):
    def run(): Unit = f

    case Day01 extends Puzzle(aoc.Day1.run())
    case Day02 extends Puzzle(aoc.Day2.run())
    case Day03 extends Puzzle(aoc.Day3.run())
    case Day04 extends Puzzle(aoc.Day4.run())
    case Day05 extends Puzzle(aoc.Day5.run())
    case Day06 extends Puzzle(aoc.Day6.run())
    case Day07 extends Puzzle(aoc.Day7.run())
    case Day08 extends Puzzle(aoc.Day8.run())
    case Day09 extends Puzzle(aoc.Day9.run())
    case Day10 extends Puzzle(aoc.Day10.run())
    case Day11 extends Puzzle(aoc.Day11.run())
    case Day12 extends Puzzle(aoc.Day12.run())
    case Day13 extends Puzzle(aoc.Day13.run())
    case Day14 extends Puzzle(aoc.Day14.run())
    case Day15 extends Puzzle(aoc.Day15.run())
    case Day16 extends Puzzle(aoc.Day16.run())
    case Day17 extends Puzzle(aoc.Day17.run())
    case Day18 extends Puzzle(aoc.Day18.run())
    case Day19 extends Puzzle(aoc.Day19.run())
    case Day20 extends Puzzle(aoc.Day20.run())
    case Day21 extends Puzzle(aoc.Day21.run())
    case Day22 extends Puzzle(aoc.Day22.run())
    case Day23 extends Puzzle(aoc.Day23.run())
    case Day24 extends Puzzle(aoc.Day24.run())
    case Day25 extends Puzzle(aoc.Day25.run())

  given FromString[Puzzle] with
    def fromString(text: String): Puzzle =
      try Puzzle.valueOf(text)
      catch
        case _: IllegalArgumentException =>
          val puzzlePattern =
            """(?:day\h?)?((?:0?[1-9])|(?:1[0-9])|(?:2[0-5]))""".r

          text.trim.toLowerCase match
            case puzzlePattern(day) =>
              try Puzzle.fromOrdinal(day.toInt - 1)
              catch
                case _: NoSuchElementException =>
                  throw new IllegalArgumentException(s"puzzle $day has not yet been solved :(")
            case _ => throw new IllegalArgumentException(s"$text is not a valid puzzle identifier")

  @main
  def runPuzzle(puzzle: Puzzle): Unit = puzzle.run()
