/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines
import aoc.extension.IteratorOps.{window, windowUntil}

import java.time.LocalDateTime

/** --- Day 4: Repose Record - Solution---
  *
  *   - Part I: 3212
  *   - Part II: 4966
  */
object Day4:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_4_input.txt")
    val logEntries = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)
      .sortBy(_.timestamp)

    // Collect windows that contain all the log entries of a single work shift.
    val workShifts = logEntries.iterator.windowUntil(_.event.isShiftStartEvent)

    val sleepingTimes = workShifts.flatMap { shiftEvents =>
      val guardId = shiftEvents.headOption
        .filter(_.event.isShiftStartEvent)
        .map {
          case GuardLogEntry(_, GuardEvent.ShiftStart(id)) => id
          case _                                           => throw new IllegalStateException()
        }
        .getOrElse(throw new UnsupportedOperationException("Expected a shift start event"))

      // Collect windows of the sleep log entries: each window contains two elements
      // 1. the sleep log entry, and
      // 2. the awake log entry, which marks the end of the sleeping time.
      val sleeping =
        shiftEvents.iterator.window(_.event.isSleepEvent, _.event.isAwakeEvent, includeClosingElement = true)
          .map {
            case e1 :: e2 :: Nil => (e1, e2)
            case logs =>
              throw new UnsupportedOperationException(s"Expected exactly two elements but found ${logs.size}")
          }

      sleeping.map {

        // Calculate how long a guard slept during the work shift.
        case (GuardLogEntry(t1, GuardEvent.Sleep), GuardLogEntry(t2, GuardEvent.Awake)) =>
          (guardId, t1.getMinute until t2.getMinute)
        case _ => throw new IllegalStateException()
      }
    }.toSeq

    // Collect the sleeping times for each guard.
    val sleepingTimesByGuardId = sleepingTimes.groupMap { (id, _) => id } { (_, minutes) => minutes }

    println(s"Part I: ${part1(sleepingTimesByGuardId)}.")
    println(s"Part II: ${part2(sleepingTimesByGuardId)}.")

  def part1(sleepingTimesByGuardId: Map[Int, Seq[Range]]): Int =
    val (guardId, sleepingTimes) = sleepingTimesByGuardId.maxBy {

      // Calculate how many minutes a guard spent asleep.
      (_, guardSleepingTimes) => guardSleepingTimes.map(sleepingMinutes => sleepingMinutes.size).sum
    }

    val minuteRange = 0 until 60
    val (chosenMinute, _) = minuteRange.map { minute =>

      // How often was the guard asleep at any minute?
      val sleepingCount = sleepingTimes.count(sleepingMinutes => sleepingMinutes.contains(minute))

      (minute, sleepingCount)
    }.maxBy { (_, count) => count }

    guardId * chosenMinute

  def part2(sleepingTimesByGuardId: Map[Int, Seq[Range]]): Int =
    val minuteRange = 0 until 60
    val sleepingCountByMinute = sleepingTimesByGuardId.iterator.flatMap { (id, sleepingTimes) =>
      minuteRange.map { minute =>

        // How often was the guard asleep at any minute?
        val sleepingCount = sleepingTimes.count(sleepingMinutes => sleepingMinutes.contains(minute))

        (id, minute, sleepingCount)
      }
    }

    val (chosenId, chosenMinute, _) = sleepingCountByMinute.maxBy { (_, _, count) => count }

    chosenId * chosenMinute

  def parseData(rawData: Iterator[String]): Seq[GuardLogEntry] =
    // Pattern should match '[1518-10-16 00:04] Guard #479 begins shift'.
    val logEntryPattern =
      """\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] (.+)""".r

    val shiftStartPattern =
      """Guard #(\d+) begins shift""".r

    rawData.map {
      case logEntryPattern(year, month, day, hour, minute, text) =>
        val timestamp = LocalDateTime.of(year.toInt, month.toInt, day.toInt, hour.toInt, minute.toInt)

        text match
          case shiftStartPattern(id) => GuardLogEntry(timestamp, GuardEvent.ShiftStart(id.toInt))
          case "falls asleep"        => GuardLogEntry(timestamp, GuardEvent.Sleep)
          case "wakes up"            => GuardLogEntry(timestamp, GuardEvent.Awake)
          case _                     => throw new IllegalArgumentException(s"Unknown event '$text'")
      case entry => throw new IllegalArgumentException(s"Could not parse '$entry'")
    }.toSeq

  enum GuardEvent:
    case ShiftStart(guardId: Int)
    case Sleep
    case Awake

    def isShiftStartEvent: Boolean =
      this match
        case ShiftStart(_) => true
        case _             => false

    def isSleepEvent: Boolean =
      this match
        case Sleep => true
        case _     => false

    def isAwakeEvent: Boolean =
      this match
        case Awake => true
        case _     => false

  case class GuardLogEntry(timestamp: LocalDateTime, event: GuardEvent)
