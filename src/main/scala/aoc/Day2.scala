/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

/** --- Day 2: Inventory Management System - Solution ---
  *
  *   - Part I: 5704
  *   - Part II: umdryabviapkozistwcnihjqx
  */
object Day2:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_2_input.txt")
    val ids = readAllLines(inputStream)(_.toSeq)
      .getOrElse(throw new IllegalArgumentException)

    // Part I
    val checksum = checksumOf(ids)
    println(s"Part I: the checksum is $checksum.")

    // Part II
    val commonLetters = findCommonLetters(ids)
    println(s"Part II: the common letters are $commonLetters.")

  def checksumOf(ids: Seq[String]): Int =
    val (twosCount, threesCount) = ids.map { id =>
      val (twos, threes) = id
        .split("")
        .groupMapReduce(letter => letter)(_ => 1)(_ + _)
        .filter { (_, letterCount) => (letterCount == 2) || (letterCount == 3) }
        .partition { (_, letterCount) => letterCount == 2 }

      val twosCount = if twos.isEmpty then 0 else 1
      val threesCount = if threes.isEmpty then 0 else 1

      (twosCount, threesCount)
    }.foldLeft((0, 0)) { case ((twos, threes), (a, b)) => (twos + a, threes + b) }

    twosCount * threesCount

  def findCommonLetters(ids: Seq[String]): String =
    def distance(id1: String, id2: String): Array[Int] =
      val id1Chars = id1.chars().toArray
      val id2Chars = id2.chars().toArray

      id1Chars.zip(id2Chars).map { (a, b) => Math.abs(a - b) }

    val (prototypeId, _, differingIndex) = ids.toList.combinations(2)
      .map {
        // Calculate the distance of the characters between two IDs.
        case id1 :: id2 :: Nil => (id1, distance(id1, id2))
        case _                 => throw new IllegalStateException()
      }

      // Select IDs which differ by exactly one character.
      .filter { (_, distance) => distance.count(_ != 0) == 1 }
      .flatMap { (candidateId, distance) =>
        // Find the index of the single differing character.
        distance.zipWithIndex
          // Find the index of the single differing character.
          .find { (dist, _) => dist != 0 }
          .map { (dist, idx) => (candidateId, dist, idx) }
      }

      // Select the ID whose distance of the differing character is the smallest.
      .minBy { (_, dist, _) => dist }

    prototypeId.toCharArray.zipWithIndex
      // Remove the single differing character from the ID.
      .filter { (_, idx) => idx != differingIndex }
      .map { (ch, _) => ch }
      .mkString
