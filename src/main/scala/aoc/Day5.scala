/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec

/** --- Day 5: Alchemical Reduction - Solution ---
  *
  *   - Part I: 11310
  *   - Part II: 6020
  */
object Day5:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_5_input.txt")
    val units = readAllLines(inputStream) { lines => lines.flatMap(_.toCharArray).toSeq }
      .getOrElse(throw new IllegalArgumentException)

    val remaining = remainingUnits(units)

    // Part I
    println(s"Part I: ${remaining.size} units remain after fully reacting the polymer.")

    val shortestPolymer = ('A' to 'Z').zip('a' to 'z').map { (unitType, inverseUnitType) =>
      // Remove all instances of a specific unit type (regardless of polarity) when processing the polymer.
      remainingUnits(remaining, unit => (unit == unitType) || (unit == inverseUnitType))
    }.minBy(_.size)

    // Part II
    println(s"Part II: the length of the shortest polymer is ${shortestPolymer.size}.")

  def remainingUnits(units: Polymer, dropUnit: Char => Boolean = _ => false): Polymer =
    def processUnits(us: Polymer): Polymer = us match
      case Seq()       => Seq()
      case vs @ Seq(u) => if dropUnit(u) then Seq() else vs
      case vs @ Seq(u1, u2) =>
        if willReact(u1, u2) || (dropUnit(u1) && dropUnit(u2)) then Seq()
        else if dropUnit(u1) then Seq(u2)
        else if dropUnit(u2) then Seq(u1)
        else vs
      case vs => remainingUnits(vs, dropUnit)

    val mid: Int = units.size / 2
    val (leftUnits, rightUnits) = units.splitAt(mid)

    val remainingLeft = processUnits(leftUnits)
    val remainingRight = processUnits(rightUnits)

    merge(remainingLeft, remainingRight)

  def merge(us: Polymer, vs: Polymer): Polymer =
    @tailrec
    def go(counter: Int): Int =
      val idxLeft = us.size - 1 - counter
      val idxRight = counter
      val isValidIndex = us.isDefinedAt(idxLeft) && vs.isDefinedAt(idxRight)

      if isValidIndex && willReact(us(idxLeft), vs(idxRight)) then go(counter + 1)
      else counter

    val dropCount = go(counter = 0)
    us.dropRight(dropCount) ++ vs.drop(dropCount)

  def willReact(unit1: Char, unit2: Char): Boolean =
    // '32' is the distance between a lowercase letter and its corresponding uppercase letter, for the letters [A-Za-z].
    // Example: The value of 'A' is 65; the value of 'a' is 97. abs(65 - 97) = 32.
    if unit1.isUpper then (unit1 + 32) == unit2
    else (unit2 + 32) == unit1

  type Polymer = Seq[Char]
