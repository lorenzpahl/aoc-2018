/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec

/** --- Day 19: Go With The Flow - Solution ---
  *
  *   - Part I: 1764
  *   - Part II: 18992484
  */
object Day19:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_19_input.txt")
    val (ipAddress, instructions) = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val device = Device(Array(Register(0), Register(0), Register(0), Register(0), Register(0), Register(0)), ipAddress)

    val finalState = run(device, instructions)
    println(s"Part I: Register 0 contains the value ${finalState(0)}.")

    val finalState2 = run(device.updated(0, 1), instructions)
    println(s"Part II: Register 0 contains the value ${finalState2(0)}.")

  def run(device: Device, instructions: IndexedSeq[Instruction]): Device =
    type IPMemory = (
        Option[InstructionPointer],
        Option[InstructionPointer],
        Option[InstructionPointer],
        Option[InstructionPointer],
        Option[InstructionPointer]
    )

    @tailrec
    def go(currentDevice: Device, currentInstructionPointer: InstructionPointer, memory: IPMemory): Device =
      if (currentInstructionPointer < 0) || (currentInstructionPointer >= instructions.size) then currentDevice
      else
        val (nextDevice, nextInstructionPointer) = memory match
          case (Some(11), Some(3), Some(4), Some(5), Some(6)) if (currentInstructionPointer == 8) =>
            currentDevice.updated(5, currentDevice(4))
              .process(instructions(currentInstructionPointer), currentInstructionPointer)
          case (Some(6), Some(8), Some(9), Some(10), Some(11)) if (currentInstructionPointer == 3) =>
            currentDevice.updated(5, Math.floorDiv(currentDevice(4), currentDevice(3)))
              .process(instructions(currentInstructionPointer), currentInstructionPointer)
          case _ => currentDevice.process(instructions(currentInstructionPointer), currentInstructionPointer)

        val (_, b, c, d, e) = memory
        val updatedMemory = (b, c, d, e, Some(currentInstructionPointer))
        go(nextDevice, nextInstructionPointer, updatedMemory)

    val initialMemory: IPMemory = (None, None, None, None, None)
    go(device, device.currentInstructionPointer, initialMemory)

  def parseData(lines: Iterator[String]): (Address, IndexedSeq[Instruction]) =
    val ipPattern =
      """#ip (\d)""".r

    val (ipAddresses, instructions) = lines.toIndexedSeq.partitionMap {
      case ipPattern(address) => Left(address.toInt)
      case rawInstruction =>
        val instruction = Opcode.values.collectFirst {
          case opcode if rawInstruction.startsWith(opcode.name) =>
            val instructionPattern =
              raw"""(${opcode.name}) (\d+) (\d+) (\d+)""".r

            rawInstruction match
              case instructionPattern(_, a, b, c) => Instruction(opcode, a.toInt, b.toInt, c.toInt)
              case _                              => throw new IllegalArgumentException()
        }.getOrElse(throw new IllegalArgumentException(s"${rawInstruction} is not a valid instruction"))

        Right(instruction)
    }

    assume(ipAddresses.sizeIs == 1, "There must be exactly one instruction pointer")
    ipAddresses.head -> instructions

  type Address = Int

  type InstructionPointer = Int

  enum Opcode(val name: String):
    case Addr extends Opcode("addr")
    case Addi extends Opcode("addi")
    case Mulr extends Opcode("mulr")
    case Muli extends Opcode("muli")
    case Banr extends Opcode("banr")
    case Bani extends Opcode("bani")
    case Borr extends Opcode("borr")
    case Bori extends Opcode("bori")
    case Setr extends Opcode("setr")
    case Seti extends Opcode("seti")
    case Gtir extends Opcode("gtir")
    case Gtri extends Opcode("gtri")
    case Gtrr extends Opcode("gtrr")
    case Eqir extends Opcode("eqir")
    case Eqri extends Opcode("eqri")
    case Eqrr extends Opcode("eqrr")

  final case class Instruction(opcode: Opcode, a: Int, b: Int, c: Int)

  final case class Register(value: Int):
    def updatedWith(newValue: Int): Register = copy(value = newValue)

  final case class Device(registers: Array[Register], ipAddress: Address):

    def apply(address: Address): Int = registers(address).value

    def updated(address: Address, newValue: Int): Device =
      Device(registers.updated(address, registers(address).updatedWith(newValue)), ipAddress)

    def currentInstructionPointer: InstructionPointer = apply(ipAddress)

    private def process(instructionPointer: InstructionPointer)(f: Device => Device): (Device, InstructionPointer) =
      val updatedDevice = updated(ipAddress, instructionPointer)
      val processedDevice = f(updatedDevice)
      processedDevice -> (processedDevice.currentInstructionPointer + 1)

    def process(instruction: Instruction, instructionPointer: InstructionPointer): (Device, InstructionPointer) =
      instruction match
        case Instruction(Opcode.Addr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra) + device.apply(rb))
          }
        case Instruction(Opcode.Addi, ra, ia, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra) + ia)
          }
        case Instruction(Opcode.Mulr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra) * device.apply(rb))
          }
        case Instruction(Opcode.Muli, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra) * ib)
          }
        case Instruction(Opcode.Banr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra) & device.apply(rb))
          }
        case Instruction(Opcode.Bani, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra) & ib)
          }
        case Instruction(Opcode.Borr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra) | device.apply(rb))
          }
        case Instruction(Opcode.Bori, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra) | ib)
          }
        case Instruction(Opcode.Setr, ra, _, rc) => process(instructionPointer) { device =>
            device.updated(rc, device.apply(ra))
          }
        case Instruction(Opcode.Seti, ia, _, rc) => process(instructionPointer) { device =>
            device.updated(rc, ia)
          }
        case Instruction(Opcode.Gtir, ia, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc, if ia > device.apply(rb) then 1 else 0)
          }
        case Instruction(Opcode.Gtri, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc, if device.apply(ra) > ib then 1 else 0)
          }
        case Instruction(Opcode.Gtrr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc, if device.apply(ra) > device.apply(rb) then 1 else 0)
          }
        case Instruction(Opcode.Eqir, ia, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc, if ia == device.apply(rb) then 1 else 0)
          }
        case Instruction(Opcode.Eqri, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc, if device.apply(ra) == ib then 1 else 0)
          }
        case Instruction(Opcode.Eqrr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc, if device.apply(ra) == device.apply(rb) then 1 else 0)
          }
