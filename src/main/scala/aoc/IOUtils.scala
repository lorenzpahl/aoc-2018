/*
 * Copyright 2020 Lorenz Pahl
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package aoc

import java.io.InputStream
import java.nio.charset.{Charset, StandardCharsets}
import scala.io.Source
import scala.util.{Try, Using}

object IOUtils:
  def readAllLines[A](is: InputStream, charset: Charset = StandardCharsets.UTF_8)(f: Iterator[String] => A): Try[A] =
    Using(Source.fromInputStream(is, charset.toString)) { source => f(source.getLines()) }

  def readChars[A](is: InputStream, charset: Charset = StandardCharsets.UTF_8)(f: Iterator[Char] => A): Try[A] =
    Using(Source.fromInputStream(is, charset.toString)) { source => f(source.iterator) }
