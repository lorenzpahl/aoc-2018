/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import scala.annotation.tailrec

/** --- Day 14: Chocolate Charts - Solution ---
  *
  *   - Part I: 2103141159
  *   - Part II: 20165733
  */
object Day14:
  def run(): Unit =
    val recipeCount = 170_641

    val scores = generateRecipes().slice(recipeCount, recipeCount + 10)
    println(s"Part I: scores of the ten recipes after the first ${recipeCount} recipes: ${scores.mkString("")}.")

    val recipeCountDigits = digitsOf(recipeCount)
    generateRecipes().sliding(recipeCountDigits.size)
      .zipWithIndex
      .collectFirst { case (slidingScores, idx) if slidingScores == recipeCountDigits => idx }
      .foreach { count =>
        println(s"Part II: ${count} recipes appear to the left of the score sequence ${recipeCount}.")
      }

  def generateRecipes(): Iterator[RecipeScore] =
    val initialScores = IndexedSeq(3, 7)
    Iterator.iterate((initialScores, initialScores, 0, 1)) { (_, scores, idxA, idxB) =>
      val scoreA = scores(idxA)
      val scoreB = scores(idxB)

      val newScores = digitsOf(scoreA + scoreB)
      val updatedScores = scores ++ newScores

      val moveCountA = 1 + scoreA
      val nextIdxA = (idxA + moveCountA) % updatedScores.size

      val moveCountB = 1 + scoreB
      val nextIdxB = (idxB + moveCountB) % updatedScores.size

      (newScores, updatedScores, nextIdxA, nextIdxB)
    }.flatMap { (newScores, _, _, _) => newScores }

  def digitsOf(num: Int): IndexedSeq[Int] =
    @tailrec
    def go(x: Int, digits: IndexedSeq[Int]): IndexedSeq[Int] =
      if x < 10 then x +: digits
      else go(x / 10, (x % 10) +: digits)

    go(num, IndexedSeq.empty)

  type RecipeScore = Int
