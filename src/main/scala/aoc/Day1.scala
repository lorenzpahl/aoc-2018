/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

/** --- Day 1: Chronal Calibration - Solution ---
  *
  *   - Part I: 466
  *   - Part II: 750
  */
object Day1:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_1_input.txt")
    val frequencyChanges = readAllLines(inputStream) { lines => lines.map(_.toLong).toSeq }
      .getOrElse(throw new IllegalArgumentException)

    // Part I
    val frequency = frequencyChanges.sum
    println(s"Part I: the resulting frequency is $frequency.")

    // Part II
    val repeatedFrequencyChanges = LazyList.continually(frequencyChanges).flatten
    val firstDuplicate = repeatedFrequencyChanges.scanLeft((Set[Long](0L), 0L, Option.empty[Long])) {
      case ((memory, acc, _), freq) =>
        val partialFreq = acc + freq

        if memory.contains(partialFreq) then (memory, partialFreq, Some(partialFreq))
        else (memory + partialFreq, partialFreq, None)
    }.flatMap { (_, _, result) => result }
      .head

    println(s"Part II: the first duplicate frequency is $firstDuplicate.")
