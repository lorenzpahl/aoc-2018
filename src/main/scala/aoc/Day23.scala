/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec
import scala.collection.mutable

/** --- Day 23: Experimental Emergency Teleportation ---
  *
  *   - Part I: 399
  *   - Part II: 81396996
  *
  * Solution for part ii is based on the following algorithm:
  * [[https://raw.githack.com/ypsu/experiments/master/aoc2018day23/vis.html]]
  */
object Day23:
  import Points.*

  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_23_input.txt")
    val nanobots = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val strongestNanobot = nanobots.maxBy(_.signalRadius)
    val reachableNanobotCount = nanobots.count { nanobot =>
      strongestNanobot.position.distanceTo(nanobot.position) <= strongestNanobot.signalRadius
    }

    println(s"Part I: ${reachableNanobotCount} nanobots are in range of the nanobot with largest signal radius.")

    part2(nanobots) match
      case None => throw new UnsupportedOperationException
      case Some(p) =>
        val dist = p.distanceTo(Point3D.ORIGIN)
        println(s"Part II: the shortest manhattan distance is ${dist}.")

  def part2(nanobots: Seq[Nanobot]): Option[Point3D] =
    extension (ps: Set[Point3D])
      def minMaxByOption(f: Point3D => Int): Option[(Point3D, Point3D)] =
        ps.foldLeft(Option.empty[(Point3D, Point3D)]) {
          case (None, p) => Some(p -> p)
          case (Some((currMin, currMax)), p) =>
            val newMin = if f(p) < f(currMin) then p else currMin
            val newMax = if f(p) > f(currMax) then p else currMax
            Some(newMin -> newMax)
        }

    val bounds = nanobots.toSet
      .flatMap { nanobot =>
        nanobot.bounds match
          case (left, right, top, bottom, back, front) => Set(left, right, top, bottom, back, front)
      }

    val (x1, x2) =
      bounds.minMaxByOption {
        case Point3D(x, _, _) => x
      }.map {
        case (Point3D(minX, _, _), Point3D(maxX, _, _)) => minX -> maxX
      } match
        case None               => throw new UnsupportedOperationException
        case Some((minX, maxX)) => minX -> maxX

    val (y1, y2) =
      bounds.minMaxByOption {
        case Point3D(_, y, _) => y
      }.map {
        case (Point3D(_, minY, _), Point3D(_, maxY, _)) => minY -> maxY
      } match
        case None               => throw new UnsupportedOperationException
        case Some((minY, maxY)) => minY -> maxY

    val (z1, z2) =
      bounds.minMaxByOption {
        case Point3D(_, _, z) => z
      }.map {
        case (Point3D(_, _, minZ), Point3D(_, _, maxZ)) => minZ -> maxZ
      } match
        case None               => throw new UnsupportedOperationException
        case Some((minZ, maxZ)) => minZ -> maxZ

    val dx = Math.abs(x1 - x2)
    val dy = Math.abs(y1 - y2)
    val dz = Math.abs(z1 - z2)
    val length = nextPowerOfTwo(Math.max(Math.max(dx, dy), dz))
    val frontUpperLeft = Point3D(x1, y1, z2)
    val backLowerRight = Point3D(x1 + length, y1 + length, z2 - length)
    val initialSearchox = Searchbox(frontUpperLeft, backLowerRight, nanobots.toSet)

    @tailrec
    def go(q: mutable.PriorityQueue[Searchbox]): Option[Point3D] =
      if q.isEmpty then Option.empty
      else
        val currentSearchbox = q.dequeue()
        if currentSearchbox.size == 1 then
          Some(currentSearchbox.ful)
        else
          for
            nextSearchbox <- currentSearchbox.split()
            if nextSearchbox.nanobotCount > 0
          do
            q.enqueue(nextSearchbox)

          go(q)
    end go

    given Ordering[Searchbox] with
      def compare(s1: Searchbox, s2: Searchbox): Int =
        if s1.nanobotCount > s2.nanobotCount then +1
        else if s1.nanobotCount < s2.nanobotCount then -1
        else if s1.distanceTo(Point3D.ORIGIN) < s2.distanceTo(Point3D.ORIGIN) then +1
        else if s1.distanceTo(Point3D.ORIGIN) > s2.distanceTo(Point3D.ORIGIN) then -1
        else if s1.size < s2.size then +1
        else if s1.size > s2.size then -1
        else 0

    val queue = new mutable.PriorityQueue[Searchbox]()
    queue.enqueue(initialSearchox)
    go(queue)
  end part2

  def parseData(lines: Iterator[String]): Seq[Nanobot] =
    val nanobotPattern =
      """^pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)$""".r

    lines.map {
      case nanobotPattern(x, y, z, r) => Nanobot(Point3D(x.toInt, y.toInt, z.toInt), r.toInt)
      case _                          => throw new IllegalArgumentException
    }.toSeq

  def nextPowerOfTwo(x: Int): Int =
    @tailrec
    def go(n: Int): Int =
      if n >= x then n
      else go(Math.multiplyExact(n, 2))

    go(1)

  case class Searchbox(ful: Point3D, blr: Point3D, nanobots: Set[Nanobot]):
    private val length: Int =
      (ful, blr) match
        case (Point3D(x1, y1, z1), Point3D(x2, y2, z2)) =>
          Math.abs(x1 - x2) ensuring (dx => dx == Math.abs(y1 - y2) && dx == Math.abs(z1 - z2))

    def size: BigInt = BigInt(length + 1).pow(3)

    def distanceTo(p: Point3D): Int = p.distanceTo(ful)

    def nanobotCount: Int = nanobots.size

    def split(): Set[Searchbox] =
      if size <= 1 then Set.empty
      else
        val delta = length / 2
        val Point3D(x, y, z) = ful

        // front, top left.
        val s1 = Searchbox(ful, Point3D(x + delta, y + delta, z - delta), Set.empty)

        // front, top right.
        val s2 = Searchbox(Point3D(x + delta, y, z), Point3D(x + 2 * delta, y + delta, z - delta), Set.empty)

        // front, bottom left.
        val s3 = Searchbox(Point3D(x, y + delta, z), Point3D(x + delta, y + 2 * delta, z - delta), Set.empty)

        // front, bottom right.
        val s4 =
          Searchbox(Point3D(x + delta, y + delta, z), Point3D(x + 2 * delta, y + 2 * delta, z - delta), Set.empty)

        // back, top left.
        val s5 = Searchbox(Point3D(x, y, z - delta), Point3D(x + delta, y + delta, z - 2 * delta), Set.empty)

        // back, top right.
        val s6 =
          Searchbox(Point3D(x + delta, y, z - delta), Point3D(x + 2 * delta, y + delta, z - 2 * delta), Set.empty)

        // back, bottom left.
        val s7 =
          Searchbox(Point3D(x, y + delta, z - delta), Point3D(x + delta, y + 2 * delta, z - 2 * delta), Set.empty)

        // back, bottom right.
        val s8 = Searchbox(
          Point3D(x + delta, y + delta, z - delta),
          Point3D(x + 2 * delta, y + 2 * delta, z - 2 * delta),
          Set.empty
        )

        Set(
          s1.copy(nanobots = nanobots.filter(s1.isInRange)),
          s2.copy(nanobots = nanobots.filter(s2.isInRange)),
          s3.copy(nanobots = nanobots.filter(s3.isInRange)),
          s4.copy(nanobots = nanobots.filter(s4.isInRange)),
          s5.copy(nanobots = nanobots.filter(s5.isInRange)),
          s6.copy(nanobots = nanobots.filter(s6.isInRange)),
          s7.copy(nanobots = nanobots.filter(s7.isInRange)),
          s8.copy(nanobots = nanobots.filter(s8.isInRange))
        )
    end split

    def isInRange(nanobot: Nanobot): Boolean =
      (ful, blr, nanobot.position) match
        case (Point3D(x1, y1, z1), Point3D(x2, y2, z2), Point3D(nx, ny, nz)) =>
          val isInside = (x1 <= nx && x2 >= nx) && (y1 <= ny && y2 >= ny) && (z1 >= nz && z2 <= nz)

          def touchesLeftSide =
            if nx < x1 then
              if (y1 <= ny && y2 >= ny) && (z1 >= nz && z2 <= nz) then x1 <= (nx + nanobot.signalRadius)
              else if y1 <= ny && y2 >= ny then
                val dist =
                  if nz > z1 then Math.abs(nz - z1)
                  else if nz < z2 then Math.abs(nz - z2)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                x1 <= (nx + r)
              else if z1 >= nz && z2 <= nz then
                val dist =
                  if ny > y2 then Math.abs(ny - y2)
                  else if ny < y1 then Math.abs(ny - y1)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                x1 <= (nx + r)
              else if (ny < y1) && (nz < z2) then
                val dist = Math.abs(ny - y1) + Math.abs(nz - z2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                x1 <= (nx + r)
              else if (ny < y1) && (nz > z1) then
                val dist = Math.abs(ny - y1) + Math.abs(nz - z1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                x1 <= (nx + r)
              else if (ny > y2) && (nz < z2) then
                val dist = Math.abs(ny - y2) + Math.abs(nz - z2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                x1 <= (nx + r)
              else if (ny > y2) && (nz > z1) then
                val dist = Math.abs(ny - y2) + Math.abs(nz - z1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                x1 <= (nx + r)
              else throw new UnsupportedOperationException
            else false

          def touchesRightSide =
            if nx > x2 then
              if (y1 <= ny && y2 >= ny) && (z1 >= nz && z2 <= nz) then x2 >= (nx - nanobot.signalRadius)
              else if y1 <= ny && y2 >= ny then
                val dist =
                  if nz > z1 then Math.abs(nz - z1)
                  else if nz < z2 then Math.abs(nz - z2)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                x2 >= (nx - r)
              else if z1 >= nz && z2 <= nz then
                val dist =
                  if ny > y2 then Math.abs(ny - y2)
                  else if ny < y1 then Math.abs(ny - y1)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                x2 >= (nx - r)
              else if (ny < y1) && (nz < z2) then
                val dist = Math.abs(ny - y1) + Math.abs(nz - z2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                x2 >= (nx - r)
              else if (ny < y1) && (nz > z1) then
                val dist = Math.abs(ny - y1) + Math.abs(nz - z1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                x2 >= (nx - r)
              else if (ny > y2) && (nz < z2) then
                val dist = Math.abs(ny - y2) + Math.abs(nz - z2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                x2 >= (nx - r)
              else if (ny > y2) && (nz > z1) then
                val dist = Math.abs(ny - y2) + Math.abs(nz - z1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                x2 >= (nx - r)
              else throw new UnsupportedOperationException
            else false

          def touchesBackSide =
            if nz < z2 then
              if (x1 <= nx && x2 >= nx) && (y1 <= ny && y2 >= ny) then z2 <= (nz + nanobot.signalRadius)
              else if x1 <= nx && x2 >= nx then
                val dist =
                  if ny > y2 then Math.abs(ny - y2)
                  else if ny < y1 then Math.abs(ny - y1)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                z2 <= (nz + r)
              else if y1 <= ny && y2 >= ny then
                val dist =
                  if nx < x1 then Math.abs(nx - x1)
                  else if nx > x2 then Math.abs(nx - x2)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                z2 <= (nz + r)
              else if (nx < x1) && (ny < y1) then
                val dist = Math.abs(nx - x1) + Math.abs(ny - y1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                z2 <= (nz + r)
              else if (nx < x1) && (ny > y2) then
                val dist = Math.abs(nx - x1) + Math.abs(ny - y2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                z2 <= (nz + r)
              else if (nx > x2) && (ny < y1) then
                val dist = Math.abs(nx - x2) + Math.abs(ny - y1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                z2 <= (nz + r)
              else if (nx > x2) && (ny > y2) then
                val dist = Math.abs(nx - x2) + Math.abs(ny - y2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                z2 <= (nz + r)
              else throw new UnsupportedOperationException
            else false

          def touchesFrontSide =
            if nz > z1 then
              if (x1 <= nx && x2 >= nx) && (y1 <= ny && y2 >= ny) then z1 >= (nz - nanobot.signalRadius)
              else if x1 <= nx && x2 >= nx then
                val dist =
                  if ny > y2 then Math.abs(ny - y2)
                  else if ny < y1 then Math.abs(ny - y1)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                z1 >= (nz - r)
              else if y1 <= ny && y2 >= ny then
                val dist =
                  if nx < x1 then Math.abs(nx - x1)
                  else if nx > x2 then Math.abs(nx - x2)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                z1 >= (nz - r)
              else if (nx < x1) && (ny < y1) then
                val dist = Math.abs(nx - x1) + Math.abs(ny - y1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                z1 >= (nz - r)
              else if (nx < x1) && (ny > y2) then
                val dist = Math.abs(nx - x1) + Math.abs(ny - y2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                z1 >= (nz - r)
              else if (nx > x2) && (ny < y1) then
                val dist = Math.abs(nx - x2) + Math.abs(ny - y1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                z1 >= (nz - r)
              else if (nx > x2) && (ny > y2) then
                val dist = Math.abs(nx - x2) + Math.abs(ny - y2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                z1 >= (nz - r)
              else throw new UnsupportedOperationException
            else false

          def touchesTopSide =
            if ny < y1 then
              if (x1 <= nx && x2 >= nx) && (z1 >= nz && z2 <= nz) then y1 <= (ny + nanobot.signalRadius)
              else if x1 <= nx && x2 >= nx then
                val dist =
                  if nz > z1 then Math.abs(nz - z1)
                  else if nz < z2 then Math.abs(nz - z2)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                y1 <= (ny + r)
              else if z1 >= nz && z2 <= nz then
                val dist =
                  if nx < x1 then Math.abs(nx - x1)
                  else if nx > x2 then Math.abs(nx - x2)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                y1 <= (ny + r)
              else if (nx < x1) && (nz > z1) then
                val dist = Math.abs(nx - x1) + Math.abs(nz - z1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                y1 <= (ny + r)
              else if (nx < x1) && (nz < z2) then
                val dist = Math.abs(nx - x1) + Math.abs(nz - z2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                y1 <= (ny + r)
              else if (nx > x2) && (nz > z1) then
                val dist = Math.abs(nx - x2) + Math.abs(nz - z1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                y1 <= (ny + r)
              else if (nx > x2) && (nz < z2) then
                val dist = Math.abs(nx - x2) + Math.abs(nz - z2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                y1 <= (ny + r)
              else throw new UnsupportedOperationException
            else false

          def touchesBottomSide =
            if ny > y2 then
              if (x1 <= nx && x2 >= nx) && (z1 >= nz && z2 <= nz) then y2 >= (ny - nanobot.signalRadius)
              else if x1 <= nx && x2 >= nx then
                val dist =
                  if nz > z1 then Math.abs(nz - z1)
                  else if nz < z2 then Math.abs(nz - z2)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                y2 >= (ny - r)
              else if z1 >= nz && z2 <= nz then
                val dist =
                  if nx < x1 then Math.abs(nx - x1)
                  else if nx > x2 then Math.abs(nx - x2)
                  else 0

                val r = Math.max(nanobot.signalRadius - dist, 0)
                y2 >= (ny - r)
              else if (nx < x1) && (nz > z1) then
                val dist = Math.abs(nx - x1) + Math.abs(nz - z1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                y2 >= (ny - r)
              else if (nx < x1) && (nz < z2) then
                val dist = Math.abs(nx - x1) + Math.abs(nz - z2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                y2 >= (ny - r)
              else if (nx > x2) && (nz > z1) then
                val dist = Math.abs(nx - x2) + Math.abs(nz - z1)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                y2 >= (ny - r)
              else if (nx > x2) && (nz < z2) then
                val dist = Math.abs(nx - x2) + Math.abs(nz - z2)
                val r = Math.max(nanobot.signalRadius - dist, 0)
                y2 >= (ny - r)
              else throw new UnsupportedOperationException
            else false

          isInside || touchesLeftSide || touchesRightSide || touchesBackSide || touchesFrontSide || touchesTopSide || touchesBottomSide
    end isInRange

  case class Nanobot(position: Point3D, signalRadius: Int):
    def bounds: (Point3D, Point3D, Point3D, Point3D, Point3D, Point3D) = position match
      case Point3D(x, y, z) =>
        val left = Point3D(x - signalRadius, y, z)
        val right = Point3D(x + signalRadius, y, z)
        val top = Point3D(x, y - signalRadius, z)
        val bottom = Point3D(x, y + signalRadius, z)
        val back = Point3D(x, y, z - signalRadius)
        val front = Point3D(x, y, z + signalRadius)

        (left, right, top, bottom, back, front)

  object Points:
    opaque type Point3D = (Int, Int, Int)

    object Point3D:
      val ORIGIN: Point3D = Point3D(0, 0, 0)

      def apply(x: Int, y: Int, z: Int): Point3D = (x, y, z)

      def unapply(p: Point3D): (Int, Int, Int) = p

    extension (p: Point3D)
      def distanceTo(q: Point3D): Int = (p, q) match
        case ((x1, y1, z1), (x2, y2, z2)) => Math.abs(x1 - x2) + Math.abs(y1 - y2) + Math.abs(z1 - z2)
  end Points
