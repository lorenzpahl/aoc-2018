/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.language.implicitConversions

/** --- Day 3: No Matter How You Slice It - Solution ---
  *
  *   - Part I: 119551
  *   - Part II: 1124
  */
object Day3:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_3_input.txt")
    val claims = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val (intersectingClaims, nonIntersectingClaims) =
      claims
        .combinations(2)
        .to(LazyList)
        .map {
          case c1 :: c2 :: Nil => (c1, c2)
          case _               => throw new IllegalStateException()
        }
        .partition { (c1, c2) => c1.intersectsWith(c2) }

    // Part I
    val area = intersectingClaims.flatMap { (c1, c2) => c1.intersectingPoints(c2) }.distinct.size

    println(s"Part I: $area square inches are within two or more claims.")

    // Part II
    val candidates = nonIntersectingClaims.flatMap { (c1, c2) => List(c1, c2) }.distinct
    val claimId =
      candidates.flatMap { candidate =>
        if candidates.filter(_ != candidate).exists(_.intersectsWith(candidate)) then None
        else Some(candidate)
      }.headOption match
        case Some(Claim(id, _, _, _, _)) => id
        case _                           => throw new IllegalStateException

    println(s"Part II: ID of non overlapping claim: $claimId.")

  def parseData(rawData: Iterator[String]): Seq[Claim] =
    // Pattern should match '#1 @ 483,830: 24x18'.
    val claimPattern =
      """#(\d+) @ (\d+),(\d+): (\d+)x(\d+)""".r

    rawData.map {
      case claimPattern(id, x, y, width, height) => Claim(id.toInt, x.toInt, y.toInt, width.toInt, height.toInt)
      case claimData                             => throw new IllegalArgumentException(s"Could not parse '$claimData'")
    }.toSeq

  case class Claim(id: Int, x: Int, y: Int, width: Int, height: Int):
    def intersectsWith(other: Claim): Boolean =
      !(((x + width <= other.x) || (other.x + other.width <= x)) ||
        ((y + height <= other.y) || (other.y + other.height <= y)))

    def intersectingPoints(other: Claim): IndexedSeq[(Int, Int)] =
      if !intersectsWith(other) then Vector()

      val intersectingX = Math.max(x, other.x)
      val intersectingY = Math.max(y, other.y)

      for
        i <- intersectingX until Math.min(x + width, other.x + other.width)
        j <- intersectingY until Math.min(y + height, other.y + other.height)
      yield (i, j)
