/*
 * Copyright 2022 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

/** --- Day 11: Chronal Charge - Solution ---
  *
  *   - Part I: 19,17
  *   - Part II: 233,288,12
  *
  * @see
  *   Summed-area table - [[https://en.wikipedia.org/wiki/Summed-area_table]]
  */
object Day11:
  private val serialNumber: Int = 7_989
  private val gridWidth: Int = 300
  private val gridHeight: Int = 300

  def run(): Unit =
    val fuelCells = buildFuelCells(serialNumber)
    val table = buildSummedAreaTable(fuelCells)
    val totalPowerAt = totalSquarePower(table)

    val squareIdentifierA = findSquareWithLargestTotalPower(totalPowerAt, Seq(3))
    println(s"Part I: 3x3 square with largest total power ${squareIdentifierA}.")

    val squareIdentifierB = findSquareWithLargestTotalPower(totalPowerAt, sizes = 1 to 300)
    println(s"Part II: square of any size with largest total power ${squareIdentifierB}.")

  type FuelCellGrid = IndexedSeq[Int]
  type SummedAreaTable = IndexedSeq[Int]
  type TotalPowerAt = (Int, Int, Int) => Int

  case class SquareIdentifier(x: Int, y: Int, size: Int)

  def findSquareWithLargestTotalPower(totalPowerAt: TotalPowerAt, sizes: Seq[Int]): SquareIdentifier =
    val nullValue = (SquareIdentifier(-1, -1, 0), Int.MinValue)
    sizes.foldLeft(nullValue)({
      case ((maxSquareIdentifier, maxPower), size) =>
        val xs = 1 to (gridWidth - size + 1)
        val ys = 1 to (gridHeight - size + 1)

        val (squareIdentifier, totalPower) = xs.flatMap(x =>
          ys.map { y =>
            SquareIdentifier(x, y, size) -> totalPowerAt(x, y, size)
          }
        ).maxBy { (_, power) => power }

        if totalPower > maxPower then squareIdentifier -> totalPower
        else maxSquareIdentifier -> maxPower
    }) match
      case (squareIdentifier, _) => squareIdentifier

  def totalSquarePower(table: SummedAreaTable)(x: Int, y: Int, size: Int): Int =
    val tableGridWidth = gridWidth + 1

    def tableValueAt(i: Int, j: Int): Int = table(j * tableGridWidth + i)

    val originX = x + size - 1
    val originY = y + size - 1

    val ia = tableValueAt(x - 1, y - 1)
    val ib = tableValueAt(originX, y - 1)
    val ic = tableValueAt(x - 1, originY)
    val id = tableValueAt(originX, originY)

    id + ia - ib - ic

  def buildSummedAreaTable(fuelCells: FuelCellGrid): SummedAreaTable =
    val auxGridWidth = gridWidth + 1
    val auxGridHeight = gridHeight + 1
    val aux = Array.fill(auxGridWidth * auxGridHeight)(0)

    def getValueAt(xs: Array[Int])(x: Int, y: Int): Int = xs(y * auxGridWidth + x)

    def setValueAt(xs: Array[Int])(value: Int, x: Int, y: Int): Unit = xs(y * auxGridWidth + x) = value

    for
      x <- 1 to gridWidth
      y <- 1 to gridHeight
    do {
      val value =
        powerLevelAt(fuelCells)(x, y) + getValueAt(aux)(x, y - 1) + getValueAt(aux)(x - 1, y) - getValueAt(aux)(
          x - 1,
          y - 1
        )
      setValueAt(aux)(value, x, y)
    }

    aux.toVector

  def buildFuelCells(serialNumber: Int): FuelCellGrid =
    def powerLevelAt(x: Int, y: Int): Int =
      val rackId = x + 10
      val p1 = rackId * y
      val p2 = p1 + serialNumber
      val p3 = p2 * rackId
      val formattedPowerLevel = f"$p3%03d"
      val i = formattedPowerLevel.length - 3
      val p4 = formattedPowerLevel.substring(i, i + 1).toInt

      p4 - 5

    val fuelCells =
      for
        y <- 1 to gridHeight
        x <- 1 to gridWidth
      yield powerLevelAt(x, y)

    fuelCells

  def powerLevelAt(grid: FuelCellGrid)(x: Int, y: Int): Int =
    val k = (y - 1) * gridWidth + (x - 1)

    grid(k)
