/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec

/** --- Day 17: Reservoir Research - Solution ---
  *
  *   - Part I: 30384
  *   - Part II: 24479
  */
object Day17:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_17_input.txt")
    val (initialGround, waterSource, yRange) = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val floodedGround = floodGround(initialGround, waterSource)
    val (groundwaterTiles, drenchedSandTiles) = floodedGround.view.flatten
      .filter {
        case Groundwater(Position(_, y)) => yRange.contains(y)
        case Sand(Position(_, y), true)  => yRange.contains(y)
        case _                           => false
      }
      .partitionMap {
        case groundwater: Groundwater => Left(groundwater)
        case drenchedSand: Sand       => Right(drenchedSand)
        case _                        => throw new IllegalStateException()
      }

    println(
      s"Part I: The total number of tiles the water can reach is ${groundwaterTiles.size + drenchedSandTiles.size}."
    )
    println(
      s"Part II: There are ${groundwaterTiles.size} water tiles left after the water spring stops producing water."
    )

  def floodGround(initialGround: Ground, waterSource: WaterSource): Ground =
    def canSettleAt(ground: Ground)(pos: Position): Boolean =
      lazy val isSettleableTile =
        ground(pos.y)(pos.x) match
          case Clay(_) | Groundwater(_) => true
          case _                        => false

      ((pos.y >= 0) && (pos.y < ground.size)) &&
      ((pos.x >= 0) && (pos.x < ground(pos.y).size)) && isSettleableTile

    def updateGroundAt(ground: Ground)(pos: Position, tile: Tile): Ground =
      ground.updated(pos.y, ground(pos.y).updated(pos.x, tile))

    @tailrec
    def tryFillLeft(ground: Ground, waterPos: Position): (Position, Ground) =
      val nextLeft = waterPos.nextLeft()
      if nextLeft.x < 0 then waterPos -> ground
      else
        ground(waterPos.y)(nextLeft.x) match
          case Clay(_) | Groundwater(_) | WaterSource(_) => waterPos -> ground
          case sand @ Sand(sandPos, _) =>
            val updatedGround = updateGroundAt(ground)(sandPos, sand.copy(drenched = true))
            updatedGround(sandPos.nextDown().y)(sandPos.x) match
              case Clay(_) | Groundwater(_) => tryFillLeft(updatedGround, sandPos)
              case Sand(_, false)           => sandPos -> tryFillDown(updatedGround, sandPos)
              case Sand(_, _)               => sandPos -> updatedGround
              case _                        => throw new IllegalStateException()

    @tailrec
    def tryFillRight(ground: Ground, waterPos: Position): (Position, Ground) =
      val nextRight = waterPos.nextRight()
      if nextRight.x >= ground(waterPos.y).size then waterPos -> ground
      else
        ground(waterPos.y)(nextRight.x) match
          case Clay(_) | Groundwater(_) | WaterSource(_) => waterPos -> ground
          case sand @ Sand(sandPos, _) =>
            val updatedGround = updateGroundAt(ground)(sandPos, sand.copy(drenched = true))
            updatedGround(sandPos.nextDown().y)(sandPos.x) match
              case Clay(_) | Groundwater(_) => tryFillRight(updatedGround, sandPos)
              case Sand(_, false)           => sandPos -> tryFillDown(updatedGround, sandPos)
              case Sand(_, _)               => sandPos -> updatedGround
              case _                        => throw new IllegalStateException()

    def fillHorizontal(ground: Ground, waterPos: Position): Ground =
      if (waterPos.x < 0) || (waterPos.x >= ground(waterPos.y).size) then ground
      else
        val (leftPos, partiallyUpdatedGround) = tryFillLeft(ground, waterPos)
        val (rightPos, updatedGround) = tryFillRight(partiallyUpdatedGround, waterPos)
        val willSettleAt = canSettleAt(updatedGround)
        val settles = (willSettleAt(leftPos.nextLeft()) && willSettleAt(leftPos.nextDown())) &&
          (willSettleAt(rightPos.nextRight()) && willSettleAt(rightPos.nextDown()))
        if settles then
          val floodedTiles = (leftPos.x to rightPos.x).view.map(x => updatedGround(waterPos.y)(x))
          floodedTiles.collect({ case Sand(sandPos, _) => Groundwater(sandPos) })
            .foldLeft(updatedGround) { (intermediateGround, groundwater) =>
              updateGroundAt(intermediateGround)(groundwater.pos, groundwater)
            }
        else updatedGround

    @tailrec
    def tryFillDown(ground: Ground, waterPos: Position): Ground =
      val nextDown = waterPos.nextDown()
      val noMoreGround = ((waterPos.y < 0) || (waterPos.y >= ground.size)) ||
        ((nextDown.y < 0) || (nextDown.y >= ground.size)) ||
        ((nextDown.x < 0) || (nextDown.x >= ground(nextDown.y).size))
      if noMoreGround then ground
      else
        ground(nextDown.y)(nextDown.x) match
          case Sand(_, true) => ground
          case sand @ Sand(sandPos, false) =>
            val updatedGround = updateGroundAt(ground)(sandPos, sand.copy(drenched = true))
            tryFillDown(updatedGround, sandPos)
          case Clay(_) | Groundwater(_) | WaterSource(_) =>
            val updatedGround = fillHorizontal(ground, waterPos)
            tryFillDown(updatedGround, waterPos.nextUp())

    tryFillDown(initialGround, waterSource.pos)

  def printGround(ground: Ground): Unit = ground.foreach(tileLayer => {
    tileLayer.foreach {
      case _: Groundwater => print("\u2592")
      case _: Clay        => print("\u2593")
      case _: WaterSource => print("\u2588")
      case Sand(_, true)  => print(" ")
      case _: Sand        => print("\u2591")
    }
    println()
  })

  def parseData(lines: Iterator[String]): (Ground, WaterSource, Range) =
    val hClayVeinPattern =
      """y=(\d+), x=(\d+)\.\.(\d+)""".r

    val vClayVeinPattern =
      """x=(\d+), y=(\d+)\.\.(\d+)""".r

    val claysByPosition = lines.flatMap({
      case hClayVeinPattern(y, xMin, xMax) => HClayVein(y.toInt, xMin.toInt to xMax.toInt).clays()
      case vClayVeinPattern(x, yMin, yMax) => VClayVein(x.toInt, yMin.toInt to yMax.toInt).clays()
    }).map(clay => clay.pos -> clay).toMap

    val nilMinMax = (Int.MaxValue, Int.MinValue)
    val ((minX, maxX), (minY, maxY)) = claysByPosition.keys.foldLeft((nilMinMax, nilMinMax)) {
      case (((intermediateMinX, intermediateMaxX), (intermediateMinY, intermediateMaxY)), Position(x, y)) =>
        val updatedMinXMaxX = Math.min(intermediateMinX, x) -> Math.max(intermediateMaxX, x)
        val updatedMinYMaxY = Math.min(intermediateMinY, y) -> Math.max(intermediateMaxY, y)

        updatedMinXMaxX -> updatedMinYMaxY
    } match
      case (`nilMinMax`, `nilMinMax`) => throw new IllegalStateException()
      case ((intermediateMinX, intermediateMaxX), (intermediateMinY, intermediateMaxY)) =>
        ((intermediateMinX - 1) -> (intermediateMaxX + 1)) -> (intermediateMinY -> intermediateMaxY)

    val waterSourcePosition = Position(500, 0)
    val ys = Math.min(0, minY) to maxY
    val xs = minX to maxX

    val ground = ys.view.map { y =>
      xs.view.map(x => Position(x, y)).map { pos =>
        val normalizedPosition = Position(pos.x - minX, pos.y)
        if pos == waterSourcePosition then WaterSource(normalizedPosition)
        else if claysByPosition.contains(pos) then claysByPosition(pos).copy(pos = normalizedPosition)
        else Sand(normalizedPosition)
      }.toIndexedSeq
    }.toIndexedSeq

    val normalizedWaterSourcePos = waterSourcePosition.copy(x = waterSourcePosition.x - minX)
    (ground, WaterSource(normalizedWaterSourcePos), minY to maxY)

  type Ground = IndexedSeq[IndexedSeq[Tile]]

  final case class Position(x: Int, y: Int):
    def nextLeft(): Position = Position(x - 1, y)

    def nextRight(): Position = Position(x + 1, y)

    def nextDown(): Position = Position(x, y + 1)

    def nextUp(): Position = Position(x, y - 1)

  sealed trait Tile:
    val pos: Position

  final case class WaterSource(pos: Position) extends Tile

  final case class Groundwater(pos: Position) extends Tile

  sealed trait Soil extends Tile

  final case class Sand(pos: Position, drenched: Boolean = false) extends Soil

  final case class Clay(pos: Position) extends Soil

  sealed trait ClayVein:
    val startPos: Position
    val endPos: Position

    def clays(): IndexedSeq[Clay] = this match
      case HClayVein(y, xs) => for x <- xs yield Clay(Position(x, y))
      case VClayVein(x, ys) => for y <- ys yield Clay(Position(x, y))

  final case class HClayVein(y: Int, xs: Range) extends ClayVein:
    override val startPos: Position = Position(xs.start, y)
    override val endPos: Position = Position(xs.end, y)

  final case class VClayVein(x: Int, ys: Range) extends ClayVein:
    override val startPos: Position = Position(x, ys.start)
    override val endPos: Position = Position(x, ys.end)
