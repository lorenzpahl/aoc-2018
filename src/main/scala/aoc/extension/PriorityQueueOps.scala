/*
 * Copyright 2020 Lorenz Pahl
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package aoc.extension

import scala.annotation.tailrec
import scala.collection.immutable.Queue
import scala.collection.mutable

object PriorityQueueOps:
  extension [A](queue: mutable.PriorityQueue[A])
    def dequeueWhile(p: (A, Int) => Boolean)(using Ordering[A]): Queue[A] =
      @tailrec
      def go(i: Int, as: Queue[A]): Queue[A] =
        if queue.nonEmpty && p(queue.max, i) then go(i + 1, as.enqueue(queue.dequeue()))
        else as

      go(0, Queue())
