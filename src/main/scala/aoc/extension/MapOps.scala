/*
 * Copyright 2021 Lorenz Pahl
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package aoc.extension

object MapOps:
  extension [K, V, W >: V](map: Map[K, V])
    def replacedWith(key: K)(replacementFunction: Option[V] => Option[(K, W)]): Map[K, W] =
      val currentValueOption = map.get(key)
      val nextEntryOption = replacementFunction(currentValueOption)
      (currentValueOption, nextEntryOption) match
        case (None, None)         => map
        case (Some(_), None)      => map.removed(key)
        case (None, Some((k, v))) => map.updated(k, v)
        case (_, Some((k, v)))    => map.removed(key).updated(k, v)
