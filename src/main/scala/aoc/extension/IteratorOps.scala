/*
 * Copyright 2021 Lorenz Pahl
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package aoc.extension

object IteratorOps:
  extension [A](as: Iterator[A])
    def windowUntil(closeWhen: A => Boolean): Iterator[Seq[A]] = window(_ => true, closeWhen)

    def window(
        openWhen: A => Boolean,
        closeWhen: A => Boolean,
        includeClosingElement: Boolean = false
    ): Iterator[Seq[A]] =
      val hasNextIter = Iterator.iterate(false)(_ => as.hasNext).drop(1)
      as.zip(hasNextIter).scanLeft((Seq.empty[A], Seq.empty[A], false)) {
        case ((bucket, _, _), (a, hasNext)) =>
          lazy val openWindow = openWhen(a)
          val closeWindow = closeWhen(a)

          if bucket.nonEmpty && closeWindow && openWindow then
            val nextWindow = if includeClosingElement then bucket :+ a else bucket
            (Seq(a), nextWindow, !hasNext)
          else if bucket.nonEmpty && closeWindow then
            val nextWindow = if includeClosingElement then bucket :+ a else bucket
            (Seq(), nextWindow, false)
          else if bucket.nonEmpty || openWindow then (bucket :+ a, Seq.empty, !hasNext)
          else (bucket, Seq.empty, bucket.nonEmpty && !hasNext)
      }.drop(1).collect {
        case (bucket, nextWindow, emitBucket) if emitBucket && nextWindow.nonEmpty => Seq(nextWindow, bucket)
        case (_, nextWindow, _) if nextWindow.nonEmpty                             => Seq(nextWindow)
        case (bucket, _, emitBucket) if emitBucket                                 => Seq(bucket)
      }.flatten
