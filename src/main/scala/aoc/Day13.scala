/*
 * Copyright 2021 Lorenz Pahl
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines
import aoc.extension.MapOps.replacedWith

import scala.annotation.tailrec

/** --- Day 13: Mine Cart Madness - Solution ---
  *
  *   - Part I: 57,104
  *   - Part II: 67,74
  */
object Day13:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_13_input.txt")
    val (trackMap, carts) = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val (firstCrashOption, lastCartOption) = runCartSimulation(trackMap, carts)
    firstCrashOption.foreach(firstCrash => println(s"Part I: first crash at location ${firstCrash}."))
    lastCartOption.foreach(lastCart => println(s"Part II: last cart at location ${lastCart.currentLocation}."))

  def runCartSimulation(trackMap: TrackMap, carts: Set[Cart]): (Option[Location], Option[Cart]) =

    @tailrec
    def nextTick(
        remainingCarts: Seq[Cart],
        cartsByCurrentLocation: Map[Location, Cart],
        firstCrashOption: Option[Location]
    ): (Set[Cart], Map[Location, Cart], Option[Location]) = remainingCarts match
      case Seq() => (cartsByCurrentLocation.values.toSet, cartsByCurrentLocation, firstCrashOption)
      case cart :: rest =>
        val nextLocation @ (nextColumnIndex, nextRowIndex) = cart.nextLocation()
        val nextSymbol = trackMap(nextRowIndex)(nextColumnIndex)

        // Ignore carts that have been removed.
        if !cartsByCurrentLocation.contains(cart.currentLocation) then
          nextTick(rest, cartsByCurrentLocation, firstCrashOption)

        // Check if a crash occurred.
        else if cartsByCurrentLocation.contains(nextLocation) then
          val updatedCartsByCurrentLocation =
            cartsByCurrentLocation.removedAll(Iterable(nextLocation, cart.currentLocation))
          nextTick(rest, updatedCartsByCurrentLocation, firstCrashOption.orElse(Some(nextLocation)))

        // Update the location of the cart.
        else
          nextSymbol match
            case track: Track =>
              val updatedCart = cart.moveTo(nextLocation, track)
              val updatedCartsByCurrentLocation = cartsByCurrentLocation.replacedWith(cart.currentLocation) { _ =>
                Some(updatedCart.currentLocation -> updatedCart)
              }

              nextTick(rest, updatedCartsByCurrentLocation, firstCrashOption)
            case _ => throw new IllegalArgumentException("Not on track")

    val initialCartLocations = carts.map(cart => cart.currentLocation -> cart).toMap
    Iterator.iterate((carts, initialCartLocations, Option.empty[Location])) {
      (cs, cartsByCurrentLocation, firstCrashOption) =>
        given Ordering[Cart] = Ordering.by(_.currentLocation.swap)

        nextTick(cs.toSeq.sorted, cartsByCurrentLocation, firstCrashOption)
    }.collectFirst({
      case (remainingCarts, _, firstCrashOption @ Some(_)) if remainingCarts.size <= 1 =>
        (firstCrashOption, remainingCarts.headOption)
    }) match
      case Some((firstCrashOption, lastCartOption)) => firstCrashOption -> lastCartOption
      case None                                     => None -> None

  def parseData(lines: Iterator[String]): (TrackMap, Set[Cart]) =
    lines.zipWithIndex.foldLeft((IndexedSeq.empty[IndexedSeq[TrackMapSymbol]], Set.empty[Cart])) {
      case ((trackMap, carts), (line, rowIndex)) =>
        val (trackRow, updatedCarts) =
          line.toCharArray.zipWithIndex.foldLeft((IndexedSeq.empty[TrackMapSymbol], carts)) {
            case ((row, cs), (trackMapSymbol, columnIndex)) =>
              val location = (columnIndex, rowIndex)
              trackMapSymbol match {
                // Cart.
                case '<' =>
                  (row :+ Track.HorizontalPath) -> (cs + Cart(location, location, Direction.Left, TurnRule.LTurn))
                case '>' =>
                  (row :+ Track.HorizontalPath) -> (cs + Cart(location, location, Direction.Right, TurnRule.LTurn))
                case '^' => (row :+ Track.VerticalPath) -> (cs + Cart(location, location, Direction.Up, TurnRule.LTurn))
                case 'v' =>
                  (row :+ Track.VerticalPath) -> (cs + Cart(location, location, Direction.Down, TurnRule.LTurn))

                // Empty space on the map.
                case symbol if symbol.isWhitespace => (row :+ Blank) -> cs

                // Track.
                case track => (row :+ Track.fromChar(track)) -> cs
              }
          }

        (trackMap :+ trackRow, updatedCarts)
    } match
      case (trackMap, carts) => trackMap -> carts

  type TrackMap = IndexedSeq[IndexedSeq[TrackMapSymbol]]

  type Location = (Int, Int)

  sealed trait TrackMapSymbol

  case object Blank extends TrackMapSymbol

  enum Track extends TrackMapSymbol:
    case VerticalPath
    case HorizontalPath
    case LeftCurve
    case RightCurve
    case Intersection

  object Track:
    def fromChar(ch: Char): Track = ch match
      case '|'  => VerticalPath
      case '-'  => HorizontalPath
      case '/'  => LeftCurve
      case '\\' => RightCurve
      case '+'  => Intersection
      case _    => throw new IllegalArgumentException(s"'${ch}' does not represent a track")

  enum Direction:
    case Up
    case Right
    case Down
    case Left

  enum TurnRule:
    case LTurn
    case RTurn
    case Straight

    def turn(currentDirection: Direction): (TurnRule, Direction) =
      this match
        case LTurn =>
          val nextDirection =
            currentDirection match
              case Direction.Up    => Direction.Left
              case Direction.Down  => Direction.Right
              case Direction.Left  => Direction.Down
              case Direction.Right => Direction.Up

          Straight -> nextDirection
        case RTurn =>
          val nextDirection =
            currentDirection match
              case Direction.Up    => Direction.Right
              case Direction.Down  => Direction.Left
              case Direction.Left  => Direction.Up
              case Direction.Right => Direction.Down

          LTurn -> nextDirection
        case Straight => RTurn -> currentDirection

  final case class Cart(origin: Location, currentLocation: Location, direction: Direction, turnRule: TurnRule):
    def nextLocation(): Location =
      val (x, y) = currentLocation
      direction match
        case Direction.Up    => (x, y - 1)
        case Direction.Down  => (x, y + 1)
        case Direction.Left  => (x - 1, y)
        case Direction.Right => (x + 1, y)

    def moveTo(location: Location, track: Track): Cart = track match
      case Track.VerticalPath | Track.HorizontalPath => copy(currentLocation = location)
      case Track.LeftCurve =>
        val nextDirection =
          direction match
            case Direction.Up    => Direction.Right
            case Direction.Down  => Direction.Left
            case Direction.Left  => Direction.Down
            case Direction.Right => Direction.Up

        copy(currentLocation = location, direction = nextDirection)
      case Track.RightCurve =>
        val nextDirection =
          direction match
            case Direction.Up    => Direction.Left
            case Direction.Down  => Direction.Right
            case Direction.Left  => Direction.Up
            case Direction.Right => Direction.Down

        copy(currentLocation = location, direction = nextDirection)
      case Track.Intersection =>
        val (nextTurnRule, nextDirection) = turnRule.turn(direction)
        copy(currentLocation = location, direction = nextDirection, turnRule = nextTurnRule)
