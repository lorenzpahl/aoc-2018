/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

/** --- Day 21: Chronal Conversion ---
  *
  *   - Part I: 202209
  *   - Part II: 11777564
  */
object Day21:
  import RAddresses.*

  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_21_input.txt")
    val (ipAddress, instructions) = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val device = Device((Register(0), Register(0), Register(0), Register(0), Register(0), Register(0)), ipAddress)

    run(device, instructions).scanLeft((Option.empty[Int], Option.empty[Int], Set.empty[Int])) {
      case ((minTerminationSignal, maxTerminationSignal, memory), (processedDevice, Instruction(Opcode.Eqrr, 5, 0, 2)))
          if processedDevice(RAddress(5)) == processedDevice(RAddress(0)) =>
        val terminationSignal = Some(processedDevice(RAddress(5)))
        (minTerminationSignal.orElse(terminationSignal), terminationSignal, Set.empty)
      case ((minTerminationSignal, maxTerminationSignal, memory), (processedDevice, Instruction(Opcode.Eqrr, 5, 0, 2)))
          if memory.contains(processedDevice(RAddress(5))) =>
        (minTerminationSignal, maxTerminationSignal, Set.empty)
      case ((minTerminationSignal, _, memory), (processedDevice, Instruction(Opcode.Eqrr, 5, 0, 2))) =>
        val terminationSignal = Some(processedDevice(RAddress(5)))
        (minTerminationSignal.orElse(terminationSignal), terminationSignal, memory + terminationSignal.value)
      case (it, _) => it
    }.drop(1).collectFirst {
      case (Some(minTerminationSignal), Some(maxTerminationSignal), memory) if memory.isEmpty =>
        minTerminationSignal -> maxTerminationSignal
    } match
      case Some(minTerminationSignal, maxTerminationSignal) =>
        println(s"Part I: lowest non-negative integer value is ${minTerminationSignal} (lower bound).")
        println(s"Part II: lowest non-negative integer value is ${maxTerminationSignal} (upper bound).")
      case _ => throw new IllegalStateException

  def run(device: Device, instructions: IndexedSeq[Instruction]): Iterator[(Device, Instruction)] =
    Iterator.iterate((device, device.currentInstructionPointer)) { (currentDevice, instructionPointer) =>
      instructions(instructionPointer) match
        case instruction @ Instruction(Opcode.Gtir, 256, 3, 2) if 256 <= currentDevice(RAddress(3)) =>
          val r3 = currentDevice(RAddress(3))
          val r2 = Math.floorDiv(r3, 256)
          val manipulatedDevice = currentDevice
            .updated(RAddress(1), 1)
            .updated(RAddress(2), r2)
            .updated(RAddress(3), r2)
            .updated(RAddress(4), 7)

          manipulatedDevice -> 8
        case instruction => currentDevice.process(instruction, instructionPointer)
    }.takeWhile { (_, instructionPointer) => instructions.isDefinedAt(instructionPointer) }
      .map { (processedDevice, instructionPointer) => processedDevice -> instructions(instructionPointer) }

  def parseData(lines: Iterator[String]): (RAddress, IndexedSeq[Instruction]) =
    val ipPattern =
      """#ip ([0-5])""".r

    val (ipAddresses, instructions) = lines.toIndexedSeq.partitionMap {
      case ipPattern(address) => Left(address.toInt)
      case rawInstruction =>
        val instruction = Opcode.values.collectFirst {
          case opcode if rawInstruction.startsWith(opcode.name) =>
            val instructionPattern =
              raw"""(${opcode.name}) (\d+) (\d+) (\d+)""".r

            rawInstruction match
              case instructionPattern(_, a, b, c) => Instruction(opcode, a.toInt, b.toInt, c.toInt)
              case _                              => throw new IllegalArgumentException()
        }.getOrElse(throw new IllegalArgumentException(s"${rawInstruction} is not a valid instruction"))

        Right(instruction)
    }

    assume(ipAddresses.sizeIs == 1, "There must be exactly one instruction pointer")
    ipAddresses.head.toRAddress -> instructions

  object RAddresses:
    opaque type RAddress = 0 | 1 | 2 | 3 | 4 | 5

    object RAddress:
      def apply(value: 0 | 1 | 2 | 3 | 4 | 5): RAddress = value

      def unapply(address: RAddress): Option[0 | 1 | 2 | 3 | 4 | 5] = Some(address)

    extension (value: Int)
      def toRAddress: RAddress =
        value match
          case address: RAddress => address
          case _                 => throw new UnsupportedOperationException

  type InstructionPointer = Int

  enum Opcode(val name: String):
    case Addr extends Opcode("addr")
    case Addi extends Opcode("addi")
    case Mulr extends Opcode("mulr")
    case Muli extends Opcode("muli")
    case Banr extends Opcode("banr")
    case Bani extends Opcode("bani")
    case Borr extends Opcode("borr")
    case Bori extends Opcode("bori")
    case Setr extends Opcode("setr")
    case Seti extends Opcode("seti")
    case Gtir extends Opcode("gtir")
    case Gtri extends Opcode("gtri")
    case Gtrr extends Opcode("gtrr")
    case Eqir extends Opcode("eqir")
    case Eqri extends Opcode("eqri")
    case Eqrr extends Opcode("eqrr")

  final case class Instruction(opcode: Opcode, a: Int, b: Int, c: Int)

  final case class Register(value: Int):
    def updatedWith(newValue: Int): Register = copy(value = newValue)

  final case class Device(registers: (Register, Register, Register, Register, Register, Register), ipAddress: RAddress):
    def apply(address: RAddress): Int =
      address match
        case RAddress(0) => registers(0).value
        case RAddress(1) => registers(1).value
        case RAddress(2) => registers(2).value
        case RAddress(3) => registers(3).value
        case RAddress(4) => registers(4).value
        case RAddress(5) => registers(5).value
        case _           => throw new IllegalStateException

    def updated(address: RAddress, newValue: Int): Device =
      (address, registers) match
        case (RAddress(0), (r0, r1, r2, r3, r4, r5)) =>
          Device((r0.updatedWith(newValue), r1, r2, r3, r4, r5), ipAddress)
        case (RAddress(1), (r0, r1, r2, r3, r4, r5)) =>
          Device((r0, r1.updatedWith(newValue), r2, r3, r4, r5), ipAddress)
        case (RAddress(2), (r0, r1, r2, r3, r4, r5)) =>
          Device((r0, r1, r2.updatedWith(newValue), r3, r4, r5), ipAddress)
        case (RAddress(3), (r0, r1, r2, r3, r4, r5)) =>
          Device((r0, r1, r2, r3.updatedWith(newValue), r4, r5), ipAddress)
        case (RAddress(4), (r0, r1, r2, r3, r4, r5)) =>
          Device((r0, r1, r2, r3, r4.updatedWith(newValue), r5), ipAddress)
        case (RAddress(5), (r0, r1, r2, r3, r4, r5)) =>
          Device((r0, r1, r2, r3, r4, r5.updatedWith(newValue)), ipAddress)
        case _ => throw new IllegalStateException

    def currentInstructionPointer: InstructionPointer = apply(ipAddress)

    private def process(instructionPointer: InstructionPointer)(f: Device => Device): (Device, InstructionPointer) =
      val updatedDevice = updated(ipAddress, instructionPointer)
      val processedDevice = f(updatedDevice)
      processedDevice -> (processedDevice.currentInstructionPointer + 1)

    def process(instruction: Instruction, instructionPointer: InstructionPointer): (Device, InstructionPointer) =
      instruction match
        case Instruction(Opcode.Addr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress) + device.apply(rb.toRAddress))
          }
        case Instruction(Opcode.Addi, ra, ia, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress) + ia)
          }
        case Instruction(Opcode.Mulr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress) * device.apply(rb.toRAddress))
          }
        case Instruction(Opcode.Muli, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress) * ib)
          }
        case Instruction(Opcode.Banr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress) & device.apply(rb.toRAddress))
          }
        case Instruction(Opcode.Bani, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress) & ib)
          }
        case Instruction(Opcode.Borr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress) | device.apply(rb.toRAddress))
          }
        case Instruction(Opcode.Bori, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress) | ib)
          }
        case Instruction(Opcode.Setr, ra, _, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, device.apply(ra.toRAddress))
          }
        case Instruction(Opcode.Seti, ia, _, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, ia)
          }
        case Instruction(Opcode.Gtir, ia, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, if ia > device.apply(rb.toRAddress) then 1 else 0)
          }
        case Instruction(Opcode.Gtri, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, if device.apply(ra.toRAddress) > ib then 1 else 0)
          }
        case Instruction(Opcode.Gtrr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, if device.apply(ra.toRAddress) > device.apply(rb.toRAddress) then 1 else 0)
          }
        case Instruction(Opcode.Eqir, ia, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, if ia == device.apply(rb.toRAddress) then 1 else 0)
          }
        case Instruction(Opcode.Eqri, ra, ib, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, if device.apply(ra.toRAddress) == ib then 1 else 0)
          }
        case Instruction(Opcode.Eqrr, ra, rb, rc) => process(instructionPointer) { device =>
            device.updated(rc.toRAddress, if device.apply(ra.toRAddress) == device.apply(rb.toRAddress) then 1 else 0)
          }
