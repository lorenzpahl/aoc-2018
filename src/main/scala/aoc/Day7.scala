/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines
import aoc.extension.PriorityQueueOps.dequeueWhile
import aoc.graph.{Digraph, Edge, Vertex}

import java.time.Duration
import scala.annotation.tailrec
import scala.collection.immutable.Queue
import scala.collection.mutable
import scala.language.implicitConversions

/** --- Day 7: The Sum of Its Parts - Solution ---
  *
  *   - Part I: FDSEGJLPKNRYOAMQIUHTCVWZXB
  *   - Part II: 1000
  */
object Day7:

  import Tasks.Task

  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_7_input.txt")
    val instructions = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val (vertices, edges) = instructions.foldLeft((Set[Vertex[String]](), Set.empty[Edge[String]])) {
      case ((vs, es), (prevStep, step)) =>
        val ws = vs + Vertex(prevStep, "Step") + Vertex(step, "Step")
        val fs = es + Edge(prevStep, step, "must be finished before")

        (ws, fs)
    }

    val graph = Digraph(vertices, edges)

    // Part I
    val (_, executionOrderA) = stepOrder(graph)(_ => Duration.ofSeconds(1), workerCount = 1)
    println(
      s"Part I: the steps should be completed in the following order: ${executionOrderA.map(_.id.toChar).mkString}."
    )

    // Part II
    val (executionTimeB, _) = stepOrder(graph)(step => Duration.ofSeconds(60 + (step.id - 64)), workerCount = 5)
    println(s"Part II: it takes ${executionTimeB.getSeconds}s to complete all steps.")

  def parseData(data: Iterator[String]): Seq[(Char, Char)] =
    val instructionPattern =
      """Step ([A-Z]) must be finished before step ([A-Z]) can begin.""".r

    data.map {
      case instructionPattern(prevStep, step) => (prevStep.charAt(0), step.charAt(0))
      case instruction                        => throw new IllegalArgumentException(s"Could not parse ${instruction}")
    }.toSeq

  def stepOrder(graph: Digraph[String, String])(timeOf: Vertex[String] => Duration, workerCount: Int)(using
      Ordering[Task[String]]
  ): (Duration, Seq[Vertex[String]]) =

    // Used to calculate how many dependencies a step has.
    val reversedGraph = graph.reverse()

    // Start processing with the steps that have no dependencies.
    val targetSteps = reversedGraph.vertices().filter(vertex => reversedGraph.adj(vertex.id).isEmpty)

    val tasks = new mutable.PriorityQueue[Task[String]]()
    for target <- targetSteps do
      tasks.enqueue(Task(0, timeOf(target).getSeconds, target))

    @tailrec
    def go(
        availableTasks: Queue[Task[String]],
        executionTime: Long,
        orderedSteps: Seq[Vertex[String]]
    ): (Long, Seq[Vertex[String]]) =
      if availableTasks.isEmpty then (executionTime, orderedSteps)
      else
        val (finishedTasks, unfinishedTasks) = availableTasks.partition {
          case Task(_, currentStepExecutionTime, _) =>
            // All tasks whose remaining processing time is <= 1 will be finished in this processing step.
            currentStepExecutionTime <= 1
        }

        val updatedUnfinishedTasks = unfinishedTasks.map {
          case Task(requirements, currentStepExecutionTime, currentStep) =>
            Task(requirements, currentStepExecutionTime - 1, currentStep)
        }

        val updatedOrderedSteps = orderedSteps ++ finishedTasks.map { case Task(_, _, currentStep) => currentStep }

        finishedTasks.flatMap {
          case Task(_, _, finishedStep) =>
            graph.adj(finishedStep.id).flatMap(nextStep => {
              val requiredSteps = reversedGraph.adj(nextStep.id)
              val unfinishedSteps = requiredSteps.count(requiredStep => !updatedOrderedSteps.contains(requiredStep))

              if unfinishedSteps > 0 then None
              else Some(Task(0, timeOf(nextStep).getSeconds, nextStep))
            })
        }.foreach(nextTask => tasks.enqueue(nextTask))

        val occupiedWorkerCount = updatedUnfinishedTasks.size
        val idleWorkerCount = workerCount - occupiedWorkerCount

        // Refill the current work queue with as many tasks as there are unoccupied workers.
        val nextTasks = tasks.dequeueWhile((_, i) => i < idleWorkerCount)

        go(updatedUnfinishedTasks.enqueueAll(nextTasks), executionTime + 1, updatedOrderedSteps)

    go(tasks.dequeueWhile((_, i) => i < workerCount), executionTime = 0, orderedSteps = Seq()) match
      case (executionTime, orderedSteps) => (Duration.ofSeconds(executionTime), orderedSteps)

  object Tasks:
    opaque type Task[A] = (Int, Long, Vertex[A])

    given [A]: Ordering[Task[A]] with
      def compare(x: Task[A], y: Task[A]): Int =
        val (c1, t1, v1) = x
        val (c2, t2, v2) = y

        if c1 < c2 then 1
        else if c1 > c2 then -1
        else if v1.id < v2.id then 1
        else if v1.id > v2.id then -1
        else if t1 < t2 then 1
        else if t1 > t2 then -1
        else 0

    object Task:
      def apply[A](requirements: Int, executionTime: Long, step: Vertex[A]): Task[A] =
        (requirements, executionTime, step)

      def unapply[A](task: Task[A]): (Int, Long, Vertex[A]) = task
  end Tasks
