/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec

/** --- Day 24: Immune System Simulator 20XX ---
  *
  *   - Part I: 14000
  *   - Part II: 6149
  */
object Day24:
  import Armies.*

  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_24_input.txt")
    val (immuneSystem, infection) = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val remainingUnits =
      fight(immuneSystem, infection) match
        case Some(winner: ImmuneSystem) => winner.army.unitCount()
        case Some(winner: Infection)    => winner.army.unitCount()
        case None                       => throw new IllegalStateException

    println(s"Part I: the winner has ${remainingUnits} units.")

    part2(immuneSystem, infection) match
      case None => throw new IllegalStateException
      case Some(winner) =>
        println(s"Part II: the immune system has ${winner.army.unitCount()} units left.")

  def part2(immuneSystem: ImmuneSystem, infection: Infection): Option[ImmuneSystem] =
    Iterator.iterate((immuneSystem, 0, false)) { (currentImmuneSystem, currBoost, _) =>
      fight(currentImmuneSystem, infection) match
        case Some(winner: ImmuneSystem) => (winner, currBoost, true)
        case _ =>
          val nextBoost = currBoost + 1
          val updatedImmuneSystem = ImmuneSystem(immuneSystem.army.applyBoost(nextBoost))
          (updatedImmuneSystem, nextBoost, false)
    }.collectFirst {
      case (winner, boost, true) => winner
    }

  def fight(immuneSystem: ImmuneSystem, infection: Infection): Option[ImmuneSystem | Infection] =
    def selectTarget(attackingGroup: Group, enemies: Set[Group]): Option[Group] =
      given Ordering[Group] = Group.mkAttackOrdering(attackingGroup)
      enemies.filterNot(_.isImmuneTo(attackingGroup.attackType)).minOption match
        case None   => Option.empty
        case target => target

    def attack(attackingGroup: Group, target: Int, army: Army): (Army, Boolean) =
      army.attack(target, attackingGroup)

    @tailrec
    def go(currentImmuneSystem: ImmuneSystem, currentInfection: Infection): Option[ImmuneSystem | Infection] =
      if !currentImmuneSystem.army.hasRemainingUnits then Some(currentInfection)
      else if !currentInfection.army.hasRemainingUnits then Some(currentImmuneSystem)
      else
        // Phase I: target selection.
        val initialImmuneSystemGroups = currentImmuneSystem.army.groups.toSet
        val initialInfectionGroups = currentInfection.army.groups.toSet
        val groups = initialImmuneSystemGroups ++ initialInfectionGroups
        val (attackingGroups, _, _) = groups.toSeq.sorted(Group.targetSelectionOrdering)
          .foldLeft((Seq.empty[(Group, Int)], initialImmuneSystemGroups, initialInfectionGroups)) {
            case ((fighters, eligibleImmuneSystemGroups, eligibleInfectionGroups), attackingGroup) =>
              attackingGroup.classification match
                case Classification.ImmuneSystem => selectTarget(attackingGroup, eligibleInfectionGroups) match
                    case Some(target) =>
                      val updatedFighters = fighters :+ (attackingGroup -> target.id)
                      val updatedInfectionGroups = eligibleInfectionGroups.diff(Set(target))
                      (updatedFighters, eligibleImmuneSystemGroups, updatedInfectionGroups)
                    case None => (fighters, eligibleImmuneSystemGroups, eligibleInfectionGroups)
                case Classification.Infection => selectTarget(attackingGroup, eligibleImmuneSystemGroups) match
                    case Some(target) =>
                      val updatedFighters = fighters :+ (attackingGroup -> target.id)
                      val updatedImmuneSystemGroups = eligibleImmuneSystemGroups.diff(Set(target))
                      (updatedFighters, updatedImmuneSystemGroups, eligibleInfectionGroups)
                    case None => (fighters, eligibleImmuneSystemGroups, eligibleInfectionGroups)
          }

        // Phase II: attacking.
        given Ordering[(Group, Int)] = Ordering.by[(Group, Int), Int]((group, _) => group.initiative).reverse
        val (nextImmuneSystem, nextInfection, hasGameAdvanced) = attackingGroups.sorted
          .foldLeft((currentImmuneSystem, currentInfection, false)) {
            case (state @ (intermediateImmuneSystem, intermediateInfection, advanced), (attackingGroup, target)) =>
              attackingGroup.classification match
                case Classification.ImmuneSystem =>
                  intermediateImmuneSystem.army.getGroupById(attackingGroup.id) match
                    case Some(currentAttackingGroup) =>
                      val (updatedArmy, hasChanged) = attack(currentAttackingGroup, target, intermediateInfection.army)
                      val updatedInfection = Infection(updatedArmy)
                      (intermediateImmuneSystem, updatedInfection, advanced || hasChanged)
                    case None => state
                case Classification.Infection =>
                  intermediateInfection.army.getGroupById(attackingGroup.id) match
                    case Some(currentAttackingGroup) =>
                      val (updatedArmy, hasChanged) =
                        attack(currentAttackingGroup, target, intermediateImmuneSystem.army)
                      val updatedImmuneSystem = ImmuneSystem(updatedArmy)
                      (updatedImmuneSystem, intermediateInfection, advanced || hasChanged)
                    case None => state
          }

        if hasGameAdvanced then go(nextImmuneSystem, nextInfection)
        else Option.empty
    end go

    go(immuneSystem, infection)
  end fight

  def parseData(lines: Iterator[String]): (ImmuneSystem, Infection) =
    val reindeerConditionPattern =
      """(\d+) units each with (\d+) hit points\s?(\(.+\))? with an attack that does (\d+) (\w+) damage at initiative (\d+)""".r

    val weaknessesImmunitiesPattern =
      """\((.*)\)""".r

    val weaknessesPattern =
      """weak to (.+)""".r

    val immunitiesPattern =
      """immune to (.+)""".r

    def parseGroup(rawGroup: String, id: Int, classification: Classification): Group =
      rawGroup match
        case reindeerConditionPattern(
              rawUnitCount,
              rawHitPoints,
              rawWeaknessesImmunities,
              rawAttackDamage,
              attackType,
              rawInitiative
            ) =>
          val (weaknesses, immunities) =
            rawWeaknessesImmunities match
              case null => Set.empty[String] -> Set.empty[String]
              case weaknessesImmunitiesPattern(rawWeaknessesImmunities) =>
                rawWeaknessesImmunities.split(";") match
                  case Array(a, b) =>
                    (a.trim, b.trim) match
                      case (weaknessesPattern(rawWeaknesses), immunitiesPattern(rawImmunities)) =>
                        rawWeaknesses.split(",").map(_.trim).toSet -> rawImmunities.split(",").map(_.trim).toSet
                      case (immunitiesPattern(rawImmunities), weaknessesPattern(rawWeaknesses)) =>
                        rawWeaknesses.split(",").map(_.trim).toSet -> rawImmunities.split(",").map(_.trim).toSet
                      case _ => throw new IllegalArgumentException
                  case Array(a) =>
                    a.trim match
                      case weaknessesPattern(rawWeaknesses) =>
                        rawWeaknesses.split(",").map(_.trim).toSet -> Set.empty[String]
                      case immunitiesPattern(rawImmunities) =>
                        Set.empty[String] -> rawImmunities.split(",").map(_.trim).toSet
                      case _ => throw new IllegalArgumentException
                  case _ => throw new IllegalArgumentException
              case _ => throw new IllegalArgumentException

          val representative =
            GUnit(rawHitPoints.toInt, rawAttackDamage.toInt, attackType, rawInitiative.toInt, weaknesses, immunities)
          Group(id, rawUnitCount.toInt, representative, classification)
        case _ => throw new IllegalArgumentException

    val (immuneSystemData, infectionData) = lines.span(_.nonEmpty)
    val immuneSystem = ImmuneSystem(Army(immuneSystemData.drop(1)
      .zipWithIndex.map { (line, id) => parseGroup(line, id + 1, Classification.ImmuneSystem) }.toSet))
    val infection = Infection(Army(infectionData.filter(_.nonEmpty).drop(1)
      .zipWithIndex.map { (line, id) => parseGroup(line, id + 1, Classification.Infection) }.toSet))

    (immuneSystem, infection)

  enum Classification:
    case ImmuneSystem
    case Infection

  case class ImmuneSystem(army: Army)

  case class Infection(army: Army)

  case class GUnit(
      hitPoints: Int,
      attackDamage: Int,
      attackType: String,
      initiative: Int,
      weaknesses: Set[String],
      immunities: Set[String]
  )

  object Armies:
    opaque type Army = Map[Int, Group]

    object Army:
      def apply(groups: Set[Group]): Army = groups.map(group => group.id -> group).toMap

    extension (army: Army)
      def attack(enemyId: Int, attackingGroup: Group): (Army, Boolean) =
        army.get(enemyId) match
          case None => army -> false
          case Some(defendingGroup) =>
            val damage = defendingGroup.computeDamage(attackingGroup)
            val defeatedUnits = Math.floorDiv(damage, defendingGroup.hitPoints)
            val remainingUnits = defendingGroup.unitCount - defeatedUnits
            if remainingUnits <= 0 then army.removed(defendingGroup.id) -> true
            else
              val hasChanged = remainingUnits != defendingGroup.unitCount
              val updatedGroup =
                Group(defendingGroup.id, remainingUnits, defendingGroup.representative, defendingGroup.classification)
              army.updated(defendingGroup.id, updatedGroup) -> hasChanged

      def groups: Iterable[Group] = army.values

      def getGroupById(groupId: Int): Option[Group] = army.get(groupId)

      def unitCount(): Int = army.values.map(_.unitCount).sum

      def hasRemainingUnits: Boolean = army.nonEmpty

      def applyBoost(boost: Int): Army = army.view.mapValues(_.withBoost(boost)).toMap
  end Armies

  case class Group private (id: Int, unitCount: Int, representative: GUnit, classification: Classification) {
    lazy val effectivePower: Int = unitCount * representative.attackDamage

    def initiative: Int = representative.initiative

    def attackType: String = representative.attackType

    def hitPoints: Int = representative.hitPoints

    def isImmuneTo(attackType: String): Boolean = representative.immunities.contains(attackType)

    def computeDamage(attackingGroup: Group): Int =
      if isImmuneTo(attackingGroup.attackType) then 0
      else if representative.weaknesses.contains(attackingGroup.attackType) then attackingGroup.effectivePower * 2
      else attackingGroup.effectivePower

    def withBoost(boost: Int): Group =
      val updatedRepresentative = representative.copy(attackDamage = representative.attackDamage + boost)
      new Group(id, unitCount, updatedRepresentative, classification)
  }

  object Group:
    def apply(id: Int, unitCount: Int, representative: GUnit, classification: Classification): Group =
      require(unitCount > 0, "Groups never have zero or negative units")
      new Group(id, unitCount, representative, classification)

    given targetSelectionOrdering: Ordering[Group] with
      def compare(first: Group, second: Group): Int =
        if first.effectivePower > second.effectivePower then -1
        else if first.effectivePower < second.effectivePower then +1
        else if first.initiative > second.initiative then -1
        else if first.initiative < second.initiative then +1
        else 0

    def mkAttackOrdering(attackingGroup: Group): Ordering[Group] = new Ordering[Group] {
      def compare(enemyGroupA: Group, enemyGroupB: Group): Int =
        val damageA = enemyGroupA.computeDamage(attackingGroup)
        val damageB = enemyGroupB.computeDamage(attackingGroup)
        if damageA > damageB then -1
        else if damageA < damageB then +1
        else if enemyGroupA.effectivePower > enemyGroupB.effectivePower then -1
        else if enemyGroupA.effectivePower < enemyGroupB.effectivePower then +1
        else if enemyGroupA.initiative > enemyGroupB.initiative then -1
        else if enemyGroupA.initiative < enemyGroupB.initiative then +1
        else 0
    }
