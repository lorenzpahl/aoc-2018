/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readChars
import aoc.graph.*

import scala.annotation.tailrec
import scala.collection.immutable.Queue
import scala.jdk.StreamConverters

/** --- Day 20: A Regular Map ---
  *
  *   - Part I: 3469
  *   - Part II: 8780
  */
object Day20:
  import GraphOps.*
  import Positions.*
  import Routes.*

  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_20_input.txt")
    val route = readChars(inputStream) { chars => parseData(chars.filterNot(_.isWhitespace)) }
      .getOrElse(throw new IllegalStateException)

    val (origin, facilityMap) = exploreFacility(route)
    val (facilityGraph, verticesByRoomPosition) = buildFacilityGraph(origin, facilityMap)
    val facilityExplorer = facilityGraph.bfs(verticesByRoomPosition(origin))

    facilityExplorer.shortestPathByMaxDoorCount match
      case Some(maxDoorCount) => println(s"Part I: shortest path requires passing through at most $maxDoorCount doors.")
      case None               => throw new IllegalStateException

    val roomCount = facilityExplorer.countRooms(doorThreshold = 1_000)
    println(s"Part II: $roomCount rooms exists that require passing through at least 1.000 doors to reach.")

  def buildFacilityGraph(
      origin: Position,
      facilityMap: Map[Position, Set[Position]]
  ): (Digraph[Position, String], Map[Position, Vertex[Position]]) =

    val verticesByRoomPosition = facilityMap.keys.zipWithIndex
      .map((roomPosition, id) => roomPosition -> Vertex(id, roomPosition)).toMap

    @tailrec
    def mkEdges(roomPositions: Seq[Position], edges: Set[Edge[String]], visited: Set[Position]): Set[Edge[String]] =
      roomPositions match
        case positions if positions.isEmpty => edges
        case roomPosition +: otherRoomPositions =>
          facilityMap.get(roomPosition) match
            case Some(connectedRooms) if connectedRooms.isEmpty || visited.contains(roomPosition) =>
              mkEdges(otherRoomPositions, edges, visited)
            case Some(connectedRooms) =>
              val srcId = verticesByRoomPosition.getOrElse(roomPosition, throw new IllegalArgumentException).id
              val connections = connectedRooms.flatMap { otherRoomPosition =>
                val destId = verticesByRoomPosition.getOrElse(otherRoomPosition, throw new IllegalArgumentException).id
                val connection = Edge(srcId, destId, s"$roomPosition connected to $otherRoomPosition")
                val reverseConnection = Edge(destId, srcId, s"$otherRoomPosition connected to $roomPosition")

                Set(connection, reverseConnection)
              }

              mkEdges(otherRoomPositions ++ connectedRooms, edges ++ connections, visited + roomPosition)
            case None => throw new IllegalArgumentException

    val vertices = verticesByRoomPosition.values.toSet
    val edges = mkEdges(Seq(origin), Set.empty, Set.empty)
    Digraph(vertices, edges) -> verticesByRoomPosition

  def exploreFacility(route: Route): (Position, Map[Position, Set[Position]]) =
    def go(
        currentPosition: Position,
        remainingRoute: Route,
        roomMap: Map[Position, Set[Position]]
    ): (Map[Position, Set[Position]], Route) =
      remainingRoute.peek match
        case (None, _) => roomMap -> Route()
        case (Some(RouteSegment.Simple(direction)), rest) =>
          val nextRoomLocation = currentPosition.atDirection(direction)
          val updatedRoomMap = roomMap.updatedWith(nextRoomLocation) {
            case None => Some(Set())
            case it   => it
          }.updatedWith(currentPosition) {
            case Some(neihbours) => Some(neihbours + nextRoomLocation)
            case _               => throw new IllegalStateException(s"Epected a room at $currentPosition")
          }

          go(nextRoomLocation, rest, updatedRoomMap)
        case (Some(segment @ RouteSegment.Branch(options)), rest) if segment.hasEmptyOption =>
          val updatedRoomMap = options.foldLeft(roomMap) { (intermediateMap, routeOption) =>
            go(currentPosition, routeOption, intermediateMap) match
              case (updatedIntermediateMap, _) => updatedIntermediateMap
          }

          go(currentPosition, rest, updatedRoomMap)
        case (Some(RouteSegment.Branch(options)), rest) =>
          val updatedRoomMap = options.foldLeft(roomMap) { (intermediateMap, routeOption) =>
            go(currentPosition, routeOption ++ rest, intermediateMap) match
              case (updatedIntermediateMap, _) => updatedIntermediateMap
          }

          updatedRoomMap -> Route()
    end go

    val origin = Position(x = 0, y = 0)
    go(origin, route, Map(origin -> Set.empty)) match
      case (facilityMap, _) => origin -> facilityMap
  end exploreFacility

  def parseData(rawData: Iterator[Char]): Route =
    def go(routeTokens: LazyList[Char], route: Route, branchBuilder: Seq[Route]): (Route, LazyList[Char]) =
      routeTokens match
        case LazyList()         => (route, LazyList())
        case '$' #:: LazyList() => (route, LazyList())
        case (token @ ('N' | 'E' | 'S' | 'W')) #:: rest =>
          val segment = RouteSegment.Simple(Direction.fromKey(token))
          go(rest, route :+ segment, branchBuilder)
        case '(' #:: rest =>
          val (subroute, remainingTokens) = go(rest, Route(), Seq.empty)
          go(remainingTokens, route ++ subroute, branchBuilder)
        case '|' #:: rest =>
          go(rest, Route(), branchBuilder :+ route)
        case ')' #:: rest =>
          val branch = RouteSegment.Branch(branchBuilder :+ route)
          (Route(branch), rest)
        case token #:: _ => throw new IllegalStateException(s"Unexpected token $token")
        case tokens      => throw new IllegalArgumentException(s"Not a valid route: $tokens")
    end go

    val tokens = rawData.to(LazyList).dropWhile(_ == '^')
    go(tokens, Route(), Seq.empty[Route]) match
      case (route, _) => route

  enum Direction:
    case North
    case East
    case South
    case West

  object Direction:
    def fromKey(key: 'N' | 'E' | 'S' | 'W'): Direction =
      key match
        case 'N' => North
        case 'E' => East
        case 'S' => South
        case 'W' => West

  object Routes:
    enum RouteSegment:
      case Simple(val direction: Direction)
      case Branch(val options: Seq[Route])

      lazy val hasEmptyOption: Boolean = this match
        case Simple(_)       => false
        case Branch(options) => options.exists(_.isEmpty)

    opaque type Route = Seq[RouteSegment]

    object Route:
      def apply(segments: RouteSegment*): Route = Seq(segments*)

      def format(route: Route): String =
        @tailrec
        def go(remainingRoute: Route, builder: StringBuilder): String =
          remainingRoute match
            case Seq() => builder.toString()
            case RouteSegment.Simple(direction) +: rest => direction match
                case Direction.North => go(rest, builder += 'N')
                case Direction.East  => go(rest, builder += 'E')
                case Direction.South => go(rest, builder += 'S')
                case Direction.West  => go(rest, builder += 'W')
            case RouteSegment.Branch(options) +: rest =>
              val formattedBranch = s"""(${options.map(Route.format(_)).mkString("|")})"""
              go(rest, builder ++= formattedBranch)

        go(route, StringBuilder())

    extension (route: Route)
      def :+(segment: RouteSegment): Route = route :+ segment

      def ++(other: Route): Route = route ++ other

      def peek: (Option[RouteSegment], Route) =
        if route.isEmpty then None -> Route()
        else route.headOption -> route.tail

      def isEmpty: Boolean = route.isEmpty

      def mkString: String = Route.format(route)
  end Routes

  object Positions:
    opaque type Position = (Int, Int)

    object Position:
      def apply(x: Int, y: Int): Position = (x, y)

      given Ordering[Position] with
        def compare(a: Position, b: Position): Int =
          (a, b) match
            case ((x1, y1), (x2, y2)) =>
              if y1 < y2 then -1
              else if y1 > y2 then +1
              else if x1 < x2 then -1
              else if x1 > x2 then +1
              else 0

    extension (pos: Position)
      def atDirection(direction: Direction): Position = direction match
        case Direction.North => pos match
            case (x, y) => (x, y - 1)
        case Direction.East => pos match
            case (x, y) => (x + 1, y)
        case Direction.South => pos match
            case (x, y) => (x, y + 1)
        case Direction.West => pos match
            case (x, y) => (x - 1, y)
  end Positions

  object GraphOps:
    class FacilityExplorer[VA, VE](verticesByOrigin: Map[Vertex[VA], (Vertex[VA], Int)]):
      def shortestPathByMaxDoorCount: Option[Int] =
        verticesByOrigin.valuesIterator.maxByOption { (_, dist) => dist } match
          case Some((_, dist)) => Some(dist)
          case None            => None

      def countRooms(doorThreshold: Int): Int =
        verticesByOrigin.valuesIterator.count { (_, dist) => dist >= doorThreshold }

    extension [VA, EA](graph: Digraph[VA, EA])
      def bfs(origin: Vertex[VA]): FacilityExplorer[VA, EA] =
        type VertexQueue = Queue[(Vertex[VA], Int)]
        type OriginMap = Map[Vertex[VA], (Vertex[VA], Int)]

        @tailrec
        def go(vertices: VertexQueue, verticesByOrigin: OriginMap): OriginMap =
          vertices.dequeueOption match
            case None => verticesByOrigin
            case Some((v, dist), remainingVertices) =>
              val (updatedVertices, updatedVerticesByOrigin) =
                graph.adj(v).foldLeft((remainingVertices, verticesByOrigin)) {
                  case (intermediateResult @ (intermediateVertices, intermediateVerticesByOrigin), w) =>
                    if intermediateVerticesByOrigin.contains(w) then intermediateResult
                    else
                      val newDist = dist + 1
                      (
                        intermediateVertices.enqueue(w -> newDist),
                        intermediateVerticesByOrigin.updated(w, v -> newDist)
                      )
                }
              go(updatedVertices, updatedVerticesByOrigin)

        new FacilityExplorer(go(Queue(origin -> 0), Map(origin -> (origin, 0))))
  end GraphOps
