/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines
import aoc.extension.MapOps.replacedWith
import aoc.graph.{Edge, Graph, Vertex}

import scala.annotation.tailrec
import scala.collection.immutable.Queue
import scala.collection.mutable
import scala.language.implicitConversions

/** --- Day 15: Beverage Bandits - Solution ---
  *
  *   - Part I: 71 * 2656 = 188576
  *   - Part II: 44 * 1298 = 57112
  */
object Day15:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_15_input.txt")
    val (gameMap, gameUnitsByPosition) = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val cavernGraph = mkCavernGraph(gameMap)

    // Part I.
    simulateCombat(cavernGraph, gameUnitsByPosition)(gameMap)(_ => false, _ => true).foreach {
      (round, result) => println(s"Part I: ${round} * ${result} = ${round * result}.")
    }

    // Part II.
    Iterator.iterate((Option.empty[(Int, Int)], 1)) { (_, offset) =>
      val gunitsByPosition = gameUnitsByPosition.view.mapValues {
        case elf: Elf => elf.copy(attackPower = elf.attackPower + offset)
        case goblin   => goblin
      }.toMap

      val combatResult = simulateCombat(cavernGraph, gunitsByPosition)(gameMap)(
        {
          case _: Elf => true
          case _      => false
        },
        remainingGunits =>
          remainingGunits.forall {
            case _: Elf => true
            case _      => false
          }
      )

      combatResult -> (offset + 1)
    }.collectFirst {
      case (Some((round, result)), _) => round -> result
    }.foreach {
      (round, result) => println(s"Part II: ${round} * ${result} = ${round * result}.")
    }

  def printGMap(gameMap: GMap, gameUnitsByPosition: Map[Position, GUnit]): Unit = gameMap.foreach { row =>
    row.foreach {
      case gobject if gameUnitsByPosition.contains(gobject.position) =>
        gameUnitsByPosition(gobject.position) match
          case _: Elf    => print("E")
          case _: Goblin => print("G")
          case null      => throw new IllegalArgumentException()
      case _: Wall   => print("#")
      case _: Cavern => print(".")
      case _         => throw new IllegalArgumentException()
    }

    println()
  }

  def simulateCombat(cavernGraph: Graph[Cavern, String], gameUnitsByPosition: Map[Position, GUnit])(gameMap: GMap)(
      looseWhen: GUnit => Boolean,
      winWhen: Iterable[GUnit] => Boolean
  ): Option[(Int, Int)] =

    def startOrder(gunits: Iterable[GUnit]): mutable.PriorityQueue[GUnit] =
      val queue = mutable.PriorityQueue[GUnit]()(GObject.orderingByPosition.reverse)
      for gunit <- gunits do queue.enqueue(gunit)

      queue

    @tailrec
    def nextRound(
        gunits: mutable.PriorityQueue[GUnit],
        gunitsByPosition: Map[Position, GUnit]
    ): (Option[Int], Map[Position, GUnit]) =
      if gunits.isEmpty then None -> gunitsByPosition
      else
        gunitsByPosition.get(gunits.dequeue().position) match

          // Don't process dead units.
          case None => nextRound(gunits, gunitsByPosition)
          case Some(gunit) => targetInRange(gunit)(cavernGraph, gunitsByPosition) match

              // At least one target in range => attack.
              case Some(target) =>
                val attackedGUnitsByPosition = gunitsByPosition.updatedWith(target.position) {
                  case Some(enemy) => Some(enemy.attacked(gunit.attackPower)).filter(_.hitPoints > 0)
                  case _           => throw new IllegalStateException()
                }

                if !attackedGUnitsByPosition.contains(target.position) && looseWhen(target) then None -> Map.empty
                else nextRound(gunits, attackedGUnitsByPosition)

              // No target in range => move.
              case _ => findNextStep(gunit)(cavernGraph, gunitsByPosition) match

                  // No target in range for this unit, but there is still a target alive.
                  case None if gunitsByPosition.exists { (_, other) => gunit.isEnemy(other) } =>
                    nextRound(gunits, gunitsByPosition)

                  // No targets remain => combat ends.
                  case None => Some(gunitsByPosition.valuesIterator.map(_.hitPoints).sum) -> gunitsByPosition

                  // Move one step towards the nearest target.
                  case Some(nextStep) =>
                    val updatedGUnit = gunit.movedTo(cavernGraph(nextStep).attr.position)
                    val updatedGUnitsByPosition = gunitsByPosition.replacedWith(gunit.position) {
                      case Some(_) => Some(updatedGUnit.position, updatedGUnit)
                      case _       => throw new IllegalStateException()
                    }

                    // After the unit has moved, check if it is in range of a target unit.
                    targetInRange(updatedGUnit)(cavernGraph, updatedGUnitsByPosition) match
                      case Some(target) =>
                        val attackedGUnitsByPosition = updatedGUnitsByPosition.updatedWith(target.position) {
                          case Some(enemy) => Some(enemy.attacked(updatedGUnit.attackPower)).filter(_.hitPoints > 0)
                          case _           => throw new IllegalStateException()
                        }

                        if !attackedGUnitsByPosition.contains(target.position) && looseWhen(target) then
                          None -> Map.empty
                        else nextRound(gunits, attackedGUnitsByPosition)
                      case _ => nextRound(gunits, updatedGUnitsByPosition)

    Iterator.iterate((Option.empty[Int], gameUnitsByPosition)) { (_, gunitsByPosition) =>
      nextRound(startOrder(gunitsByPosition.values), gunitsByPosition)
    }.zipWithIndex.collectFirst {
      case ((None, gunitsByPosition), _) if gunitsByPosition.isEmpty                     => None
      case ((Some(result), gunitsByPosition), round) if winWhen(gunitsByPosition.values) => Some((round - 1, result))
    }.flatten

  def findNextStep(gunit: GUnit)(
      cavernGraph: Graph[Cavern, String],
      gunitsByPosition: Map[Position, GUnit]
  ): Option[VertexId] =
    @tailrec
    def go(
        queue: Queue[((Cavern, Cavern), Int)],
        visited: Set[VertexId],
        stepCountsByCandidate: Map[Cavern, Option[(Cavern, Int)]]
    ): Map[Cavern, Option[(Cavern, Int)]] =
      queue.dequeueOption match
        case None => stepCountsByCandidate
        case Some(((origin, nextCavern), stepCount) -> remaining) if stepCountsByCandidate.contains(nextCavern) =>
          val updatedStepCountsByCandidate = stepCountsByCandidate.updatedWith(nextCavern) {
            case Some(None)                                                  => Some(Some(origin -> stepCount))
            case Some(Some(_ -> prevStepCount)) if stepCount < prevStepCount => Some(Some(origin -> stepCount))
            case Some(Some(prevOrigin -> prevStepCount))
                if (stepCount == prevStepCount) && GObject.orderingByPosition.lt(origin, prevOrigin) =>
              Some(Some(origin -> stepCount))
            case currentValue @ Some(_) => currentValue
            case None                   => throw new IllegalStateException()
          }

          go(remaining, visited, updatedStepCountsByCandidate)
        case Some(((origin, nextCavern), stepCount) -> remaining) =>
          val neighborCaverns = cavernGraph.adj(nextCavern.vertexId)
            .filterNot(neighbour => visited.contains(neighbour.id))
            .collect {
              case Vertex(_, cavern) if !gunitsByPosition.contains(cavern.position) => cavern
            }

          val updatedVisited = visited ++ neighborCaverns.filterNot(stepCountsByCandidate.contains).map(_.vertexId)
          val nextQueueEntries = neighborCaverns.map(cavern => (origin, cavern) -> (stepCount + 1))
          val updatedQueue = remaining.enqueueAll(nextQueueEntries)

          go(updatedQueue, updatedVisited, stepCountsByCandidate)

    // Find all open, adjacent squares of the gunit.
    val stepCountsByCandidate = cavernGraph.adj(gunit.vertexId)
      .collect { case Vertex(_, cavern) if !gunitsByPosition.contains(cavern.position) => cavern -> None }
      .toMap

    // Find all open, adjacent squares of the gunit's enemies.
    val enemies = gunitsByPosition.filter { (_, otherGUnit) => gunit.isEnemy(otherGUnit) }
    val targetSquaresInRange = enemies.flatMap { (_, enemy) =>
      cavernGraph.adj(enemy.vertexId)
        .collect { case Vertex(_, cavern) if !gunitsByPosition.contains(cavern.position) => cavern }
    }

    val initialQueue = targetSquaresInRange.map(cavern => (cavern, cavern) -> 0).to(Queue)
    val initialVisited = targetSquaresInRange.map(_.vertexId).toSet

    // Find the shortest path from a target square to the gunit.
    val ord = Ordering.by[(Cavern, Int), Int] { (_, stepCount) => stepCount }
      .orElseBy { (cavern, _) => cavern }(GObject.orderingByPosition)

    go(initialQueue, initialVisited, stepCountsByCandidate).collect {
      case (cavern, Some((_, stepCount))) => cavern -> stepCount
    }.minOption(ord).map { (cavern, _) => cavern.vertexId }

  def targetInRange(gunit: GUnit)(
      cavernGraph: Graph[Cavern, String],
      gunitsByPosition: Map[Position, GUnit]
  ): Option[GUnit] =
    cavernGraph.adj(gunit.vertexId)
      .flatMap(neighbour => gunitsByPosition.get(neighbour.attr.position))
      .collect { case other if gunit.isEnemy(other) => other }
      .minOption(GUnit.orderingByHitPoints)

  def mkCavernGraph(gameMap: GMap): Graph[Cavern, String] =
    val (vertices, edges) = gameMap.flatten
      .collect { case cavern: Cavern => cavern }
      .foldLeft((Set.empty[Vertex[Cavern]], Set.empty[Edge[String]])) {
        case ((vs, es), cavern) =>
          val (y, x) = cavern.position
          val rightConnection = Some((y, x + 1))
            .filter { (ry, rx) => gameMap.isDefinedAt(ry) && gameMap(ry).isDefinedAt(rx) }
            .map { (ry, rx) => gameMap(ry)(rx) }
            .collect { case cavern: Cavern => cavern.vertexId }
            .map(rid => Edge(cavern.vertexId, rid, "adjacent to"))

          val bottomConnection = Some((y + 1, x))
            .filter { (by, bx) => gameMap.isDefinedAt(by) && gameMap(by).isDefinedAt(bx) }
            .map { (by, bx) => gameMap(by)(bx) }
            .collect { case cavern: Cavern => cavern.vertexId }
            .map(bid => Edge(cavern.vertexId, bid, "adjacent to"))

          (vs + Vertex(cavern.vertexId, cavern)) -> (es ++ rightConnection ++ bottomConnection)
      }

    Graph(vertices, edges)

  def parseData(lines: Iterator[String]): (GMap, Map[Position, GUnit]) =
    val (gameMap, gameUnitsByPosition) =
      lines.zipWithIndex.foldLeft((IndexedSeq.empty[IndexedSeq[GObject]], Map.empty[Position, GUnit])) {
        case ((gmap, gunits), (line, row)) =>
          val (gos, rus) =
            line.toCharArray.zipWithIndex.foldLeft((IndexedSeq.empty[GObject], Map.empty[Position, GUnit])) {
              case ((mapRow, rowUnitsByPosition), (ch, column)) => ch match
                  case '#' => (mapRow :+ Wall((row, column))) -> rowUnitsByPosition
                  case '.' => (mapRow :+ Cavern((row, column))) -> rowUnitsByPosition
                  case 'G' =>
                    val position = (row, column)
                    val goblin = Goblin(position, 3, 200)
                    (mapRow :+ Cavern(position)) -> (rowUnitsByPosition + (position -> goblin))
                  case 'E' =>
                    val position = (row, column)
                    val elf = Elf(position, 3, 200)
                    (mapRow :+ Cavern(position)) -> (rowUnitsByPosition + (position -> elf))
            }

          (gmap :+ gos) -> (gunits ++ rus)
      }

    gameMap -> gameUnitsByPosition

  type GMap = IndexedSeq[IndexedSeq[GObject]]

  type Position = (Int, Int)

  type VertexId = Int

  type Path = Seq[VertexId]

  sealed trait GObject:
    val position: Position

    lazy val vertexId: VertexId =
      position match
        case (y, x) => y * 500 + x

  object GObject:
    def orderingByPosition[A <: GObject]: Ordering[A] = Ordering.by(_.position)

  case class Wall(position: Position) extends GObject

  case class Cavern(position: Position) extends GObject

  sealed trait GUnit extends GObject:
    type S <: GUnit

    val attackPower: Int
    val hitPoints: Int

    def isEnemy(other: GUnit): Boolean

    def attacked(attackPower: Int): S

    def movedTo(position: Position): S

  object GUnit:
    def orderingByHitPoints[A <: GUnit]: Ordering[A] = Ordering.by[A, Int](_.hitPoints).orElseBy(_.position)

  case class Elf(position: Position, attackPower: Int, hitPoints: Int) extends GUnit:
    override type S = Elf

    override def isEnemy(other: GUnit): Boolean =
      other match
        case _: Goblin => true
        case _         => false

    override def attacked(attackPower: Int): Elf = copy(hitPoints = hitPoints - attackPower)

    override def movedTo(position: Position): Elf = copy(position = position)

  case class Goblin(position: Position, attackPower: Int, hitPoints: Int) extends GUnit:
    override type S = Goblin

    override def isEnemy(other: GUnit): Boolean =
      other match
        case _: Elf => true
        case _      => false

    override def attacked(attackPower: Int): Goblin = copy(hitPoints = hitPoints - attackPower)

    override def movedTo(position: Position): Goblin = copy(position = position)
