/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec
import scala.collection.mutable

/** --- Day 22: Mode Maze ---
  *
  *   - Part I: 5786
  *   - Part II: 986
  */
object Day22:
  import CaveSystems.*
  import Coordinates.*

  def run(): Unit =
    given CaveContext =
      CaveContext(depth = 9_171, mouth = Coordinate(x = 0, y = 0), target = Coordinate(x = 7, y = 721))
    // given CaveContext = CaveContext(depth = 510, mouth = Coordinate(x = 0, y = 0), target = Coordinate(x = 10, y = 10))

    val caveSystem = CaveSystem.upToIncludingTarget
    val totalRiskLevel = calculateTotalRiskLevel(caveSystem)
    println(s"Part I: the total risk level is ${totalRiskLevel}.")

    val pastTime = PathFinder.findTarget(caveSystem).getOrElse(throw new IllegalStateException)
    println(s"Part II: the target can be reached in ${pastTime} minutes.")

  def calculateTotalRiskLevel(caveSystem: CaveSystem)(using context: CaveContext): Int =
    caveSystem.regionsWithin(upperStart = context.mouth, lowerEnd = context.target)
      .map { (region, _) => region.riskLevel }
      .sum

  def printCaveSystem(caveSystem: CaveSystem)(using context: CaveContext): Unit =
    caveSystem.foreachLayer { regions =>
      regions.foreach {
        case (_, coordinate) if coordinate == context.mouth  => print('M')
        case (_, coordinate) if coordinate == context.target => print('T')
        case (_: Region.Rocky, _)                            => print('.')
        case (_: Region.Wet, _)                              => print('=')
        case (_: Region.Narrow, _)                           => print('|')
      }

      println()
    }

    println()

  case class CaveContext(depth: Int, mouth: Coordinate, target: Coordinate)

  object PathFinder:
    def findTarget(caveSystem: CaveSystem)(using context: CaveContext): Option[Int] =
      type ClosedMap = mutable.Map[(Coordinate, Option[Equipment]), Int]

      @tailrec
      def go(
          queue: mutable.PriorityQueue[RouteSegment],
          currentCaveSystem: CaveSystem,
          closedMap: ClosedMap
      ): Option[Int] =
        if queue.isEmpty then None
        else
          queue.dequeue match
            case routeSegment if isTargetAt(routeSegment) => Some(routeSegment.pastTime)
            case closedRouteSegment =>
              closedMap.updateWith(closedRouteSegment.identifier) {
                case None => Some(closedRouteSegment.pastTime)
                case Some(bestTime) if closedRouteSegment.pastTime < bestTime =>
                  Some(closedRouteSegment.pastTime)
                case it => it
              }

              val (nextCandidates, updatedCaveSystem) = nextRouteSegmentsFor(closedRouteSegment, currentCaveSystem)
              val newCandidates = nextCandidates.filter { candidate =>
                closedMap.get(candidate.identifier) match
                  case None           => true
                  case Some(bestTime) => candidate.pastTime < bestTime
              }

              go(queue.addAll(newCandidates), updatedCaveSystem, closedMap)
      end go

      val startRouteSegment = RouteSegment(context.mouth, Some(Equipment.Torch), 0)
      val initialQueue = mutable.PriorityQueue(startRouteSegment)(summon[Ordering[RouteSegment]].reverse)
      go(initialQueue, caveSystem, mutable.Map.empty)
    end findTarget

    def nextRouteSegmentsFor(routeSegment: RouteSegment, caveSystem: CaveSystem)(using
        CaveContext
    ): (Iterable[RouteSegment], CaveSystem) =
      def nextNeighboursFor(routeSegment: RouteSegment): (Iterable[RouteSegment], CaveSystem) =
        routeSegment match
          case RouteSegment(coordinate, equipmentOption, pastTime) =>
            val (neighbours, updatedCaveSystem) = caveSystem.adjacentTo(coordinate)
            val neighbourRouteSegments = neighbours.filter { (neighbourRegion, _) =>
              equipmentOption match
                case Some(equipment) =>
                  equipment match
                    case Equipment.Torch        => neighbourRegion.isRocky || neighbourRegion.isNarrow
                    case Equipment.ClimbingGear => neighbourRegion.isRocky || neighbourRegion.isWet
                case None => neighbourRegion.isWet || neighbourRegion.isNarrow
            }.map { (_, neighbourCoordinate) => RouteSegment(neighbourCoordinate, equipmentOption, pastTime + 1) }

            neighbourRouteSegments -> updatedCaveSystem

      def changeEquipmentAt(routeSegment: RouteSegment): Iterable[RouteSegment] =
        def validEquipmentFor(region: Region): Iterable[Option[Equipment]] = region match
          case _: Region.Rocky  => Iterable(Some(Equipment.Torch), Some(Equipment.ClimbingGear))
          case _: Region.Wet    => Iterable(Some(Equipment.ClimbingGear), None)
          case _: Region.Narrow => Iterable(Some(Equipment.Torch), None)

        routeSegment match
          case RouteSegment(coordinate, equipmentOption, pastTime) => caveSystem.getAt(coordinate) match
              case region => validEquipmentFor(region).filterNot { _ == equipmentOption }
                  .map { RouteSegment(coordinate, _, pastTime + 7) }

      val (neighbourRouteSegments, updatedCaveSystem) = nextNeighboursFor(routeSegment)
      (changeEquipmentAt(routeSegment) ++ neighbourRouteSegments) -> updatedCaveSystem
    end nextRouteSegmentsFor

    def isTargetAt(routeSegment: RouteSegment)(using context: CaveContext): Boolean =
      routeSegment match
        case RouteSegment(coordinate, Some(Equipment.Torch), _) => coordinate == context.target
        case _                                                  => false

    case class RouteSegment(coordinate: Coordinate, equipment: Option[Equipment], pastTime: Int):
      val identifier: (Coordinate, Option[Equipment]) = coordinate -> equipment

    given routeSegmentOrdering(using context: CaveContext): Ordering[RouteSegment] with
      def compare(segmentA: RouteSegment, segmentB: RouteSegment): Int =
        (segmentA, segmentB) match
          case (RouteSegment(Coordinate(x1, y1), _, time1), RouteSegment(Coordinate(x2, y2), _, time2)) =>
            val Coordinate(targetX, targetY) = context.target
            val dx1 = Math.abs(x1 - targetX)
            val dx2 = Math.abs(x2 - targetX)
            val dy1 = Math.abs(y1 - targetY)
            val dy2 = Math.abs(y2 - targetY)
            val dist1 = dx1 + dy1 + time1
            val dist2 = dx2 + dy2 + time2

            if dist1 < dist2 then -1
            else if dist1 > dist2 then +1
            else 0
  end PathFinder

  object CaveSystems:
    opaque type CaveSystem = IndexedSeq[IndexedSeq[Region]]

    object CaveSystem:
      def upToIncludingTarget(using context: CaveContext): CaveSystem =
        val nilCaveSystem: CaveSystem = IndexedSeq.empty
        nilCaveSystem.expandTo(context.target)

      extension (caveSystem: CaveSystem)
        def getAt(coordinate: Coordinate): Region = coordinate match
          case Coordinate(x, y) => caveSystem(y)(x)

        def regionsWithin(upperStart: Coordinate, lowerEnd: Coordinate): Iterator[(Region, Coordinate)] =
          val Coordinate(startX, startY) = upperStart
          val Coordinate(endX, endY) = lowerEnd

          caveSystem.slice(startY, endY + 1).iterator.zipWithIndex
            .flatMap { (regionLayer, y) =>
              regionLayer.slice(startX, endX + 1).iterator.zipWithIndex
                .map { (region, x) => region -> Coordinate(x, y) }
            }

        def adjacentTo(coordinate: Coordinate)(using CaveContext): (Iterable[(Region, Coordinate)], CaveSystem) =
          def expansionCoordinate(a: Coordinate, b: Coordinate): Coordinate = (a, b) match
            case (Coordinate(x1, y1), Coordinate(x2, y2)) => Coordinate(Math.max(x1, x2), Math.max(y1, y2))

          val updatedCaveSystem = expandTo(expansionCoordinate(coordinate.atRight, coordinate.atBottom))
          val topNeighbourOption = coordinate.atTop
            .map(topCoordinate => updatedCaveSystem.getAt(topCoordinate) -> topCoordinate)

          val leftNeighbourOption = coordinate.atLeft
            .map(leftCoordinate => updatedCaveSystem.getAt(leftCoordinate) -> leftCoordinate)

          val bottomCoordinate = coordinate.atBottom
          val bottomNeighbour = updatedCaveSystem.getAt(bottomCoordinate) -> bottomCoordinate

          val rightCoordinate = coordinate.atRight
          val rightNeighbour = updatedCaveSystem.getAt(rightCoordinate) -> rightCoordinate

          val neighboursByDirection =
            Iterable(bottomNeighbour, rightNeighbour) ++ topNeighbourOption ++ leftNeighbourOption
          neighboursByDirection -> updatedCaveSystem

        private def expandTo(coordinate: Coordinate)(using CaveContext): CaveSystem = coordinate match
          case Coordinate(x, y) if caveSystem.isDefinedAt(y) && caveSystem(y).isDefinedAt(x) => caveSystem
          case Coordinate(targetX, targetY) =>
            val sizeDiff = if caveSystem.sizeIs > targetY then 0 else Math.abs(caveSystem.size - (targetY + 1))
            val newLayer = Vector.fill(sizeDiff)(Vector.empty[Region])
            val updatedCaveSystem = caveSystem ++ newLayer

            assume(updatedCaveSystem.nonEmpty)
            updatedCaveSystem.indices.foldLeft(updatedCaveSystem) { (partialCaveSystem, y) =>
              val nextX = updatedCaveSystem(y).size
              ((nextX to targetX).foldLeft(partialCaveSystem) { (intermediateCaveSystem, x) =>
                val geologicIndex = intermediateCaveSystem.geologicIndexAt(Coordinate(x, y))
                val region = Region.fromGeologicIndex(geologicIndex)
                val intermediateXLane = intermediateCaveSystem(y)

                intermediateCaveSystem.updated(y, intermediateXLane :+ region)
              }) ensuring { _(y).nonEmpty }
            }

        private def geologicIndexAt(coordinate: Coordinate)(using context: CaveContext): Int =
          coordinate match
            case _ if coordinate == context.mouth || coordinate == context.target => 0
            case Coordinate(x, 0)                                                 => x * 16_807
            case Coordinate(0, y)                                                 => y * 48_271
            case Coordinate(x, y) =>
              caveSystem.getAt(Coordinate(x - 1, y)).erosionLevel * caveSystem.getAt(Coordinate(x, y - 1)).erosionLevel

        def foreachLayer(consumer: (Iterator[(Region, Coordinate)]) => Unit): Unit =
          caveSystem.iterator.zipWithIndex.foreach { (regionLayer, y) =>
            consumer(regionLayer.iterator.zipWithIndex
              .map { (region, x) => region -> Coordinate(x, y) })
          }
  end CaveSystems

  enum Equipment:
    case Torch
    case ClimbingGear

  enum Region(val geologicIndex: Int, val erosionLevel: Int):
    def riskLevel: 0 | 1 | 2 =
      this match
        case _: Rocky  => 0
        case _: Wet    => 1
        case _: Narrow => 2

    def isRocky: Boolean =
      this match
        case _: Rocky => true
        case _        => false

    def isWet: Boolean =
      this match
        case _: Wet => true
        case _      => false

    def isNarrow: Boolean =
      this match
        case _: Narrow => true
        case _         => false

    case Rocky(override val geologicIndex: Int, override val erosionLevel: Int)
        extends Region(geologicIndex, erosionLevel)

    case Wet(override val geologicIndex: Int, override val erosionLevel: Int)
        extends Region(geologicIndex, erosionLevel)

    case Narrow(override val geologicIndex: Int, override val erosionLevel: Int)
        extends Region(geologicIndex, erosionLevel)

  object Region:
    def fromGeologicIndex(geologicIndex: Int)(using context: CaveContext): Region =
      val erosionLevel = Math.floorMod(geologicIndex + context.depth, 20_183)
      Math.floorMod(erosionLevel, 3) match
        case 0 => Rocky(geologicIndex, erosionLevel)
        case 1 => Wet(geologicIndex, erosionLevel)
        case 2 => Narrow(geologicIndex, erosionLevel)
        case _ => throw new IllegalStateException

  object Coordinates:
    opaque type Coordinate = (Int, Int)

    object Coordinate:
      def apply(x: Int, y: Int): Coordinate =
        require(x >= 0, s"$x must be greater or equal to zero")
        require(y >= 0, s"$y must be greater or equal to zero")
        (x, y)

      def unapply(coordinate: Coordinate): (Int, Int) = coordinate

    extension (coordinate: Coordinate)
      def atTop: Option[Coordinate] = coordinate match
        case Coordinate(x, 0) => None
        case Coordinate(x, y) => Some(Coordinate(x, y - 1))

      def atLeft: Option[Coordinate] = coordinate match
        case Coordinate(0, y) => None
        case Coordinate(x, y) => Some(Coordinate(x - 1, y))

      def atRight: Coordinate = coordinate match
        case Coordinate(x, y) => Coordinate(x + 1, y)

      def atBottom: Coordinate = coordinate match
        case Coordinate(x, y) => Coordinate(x, y + 1)
  end Coordinates
