/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines
import aoc.extension.IteratorOps.windowUntil

import java.time.Duration
import scala.annotation.tailrec
import scala.language.implicitConversions

/** --- Day 10: The Stars Align - Solution ---
  *
  *   - Part I: JLPZFJRH
  *   - Part II: 10595
  */
object Day10:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_10_input.txt")
    val lightPoints = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val (message, duration) = findMessage(lightPoints)
    println(s"Message appears after ${duration.getSeconds}s")
    printMessage(message)

  def findMessage(lightPoints: Seq[LightPoint]): (Seq[LightPoint], Duration) =

    // Check if each possible letter has the same height.
    def isPossibleMessage(pss: Seq[Seq[LightPoint]]): Boolean =
      val yss = pss.map { ps =>
        val minY = ps.minBy(_.posY).posY
        val maxY = ps.maxBy(_.posY).posY

        (minY, maxY)
      }

      val (referenceMinYOption, referenceMaxYOption) = yss.headOption.unzip
      referenceMinYOption.zip(referenceMaxYOption).forall { (referenceMinY, referenceMaxY) =>
        yss.forall { (minY, maxY) => (minY == referenceMinY) && (maxY == referenceMaxY) }
      }

    // Check if a window could represent a letter.
    def isPossibleLetter(ps: Seq[LightPoint]): Boolean =
      val ys = ps.map(_.posY).sorted
      val adjacentYs = ys.zip(ys.headOption ++ ys)

      adjacentYs.forall { (y1, y2) => Math.abs(y1 - y2) <= 1 }

    @tailrec
    def go(ps: Seq[LightPoint], time: Int): (Seq[LightPoint], Int) =
      val sortedPoints = ps.sorted(LightPointOrdering)
      val adjacentPoints = sortedPoints.zip(sortedPoints.headOption ++ sortedPoints)

      // Creates windows where each window represents a possible letter. Points that create a letter have to be next
      // to each other.
      val pss = adjacentPoints.iterator.windowUntil { (p, q) => Math.abs(p.posX - q.posX) > 1 }
        .map(ws => ws.map { (p, _) => p }).toSeq

      val passMessageCheck = isPossibleMessage(pss)
      lazy val passLetterCheck = pss.forall(isPossibleLetter)

      if passMessageCheck && passLetterCheck then ps -> time
      else go(ps.map(_.move()), time + 1)

    go(lightPoints, time = 0) match
      case (ps, time) => (ps, Duration.ofSeconds(time))

  def printMessage(lightPoints: Seq[LightPoint]): Unit =
    val sortedPoints = lightPoints.sorted(PrintableLightPointOrdering)
    val prevPoints = sortedPoints.headOption ++ sortedPoints
    val adjacentPoints = sortedPoints.zip(prevPoints)

    val pss = adjacentPoints.iterator.windowUntil { (p, q) => p.posY != q.posY }
      .map(ps => ps.map { (p, _) => p })

    val minXOption = sortedPoints.minByOption(_.posX).map(p => p.posX - 4)
    val maxXOption = sortedPoints.maxByOption(_.posX).map(p => p.posX + 4)
    val xsOption = minXOption.zip(maxXOption).map { (minX, maxX) => minX to maxX }

    xsOption.foreach { xs =>
      pss.foreach { ps =>
        xs.map { i =>
          if ps.exists(_.posX == i) then "\u2593"
          else "\u2591"
        }.foreach(print)

        println()
      }
    }

  def parseData(data: Iterator[String]): Seq[LightPoint] =
    val pointPattern =
      """position=<\s*(-?\d+),\s*(-?\d+)> velocity=<\s*(-?\d+),\s*(-?\d+)>""".r

    data.map {
      case pointPattern(posX, posY, velocityX, velocityY) =>
        LightPoint(posX.toInt, posY.toInt, velocityX.toInt, velocityY.toInt)
      case line => throw new IllegalArgumentException(s"Could not parse '$line'")
    }.toSeq

  case class LightPoint(posX: Int, posY: Int, velocityX: Int, velocityY: Int):
    def move(speedFactor: Int = 1): LightPoint =
      LightPoint(posX + (velocityX * speedFactor), posY + (velocityY * speedFactor), velocityX, velocityY)

  object LightPointOrdering extends Ordering[LightPoint]:
    override def compare(p: LightPoint, q: LightPoint): Int =
      if p.posX < q.posX then -1
      else if p.posX > q.posX then 1
      else if p.posY < q.posY then -1
      else if p.posY > q.posY then 1
      else 0

  object PrintableLightPointOrdering extends Ordering[LightPoint]:
    override def compare(p: LightPoint, q: LightPoint): Int =
      if p.posY < q.posY then -1
      else if p.posY > q.posY then 1
      else if p.posX < q.posX then -1
      else if p.posX > q.posX then 1
      else 0
