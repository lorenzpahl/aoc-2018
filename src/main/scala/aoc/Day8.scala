/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec

/** --- Day 8: Memory Maneuver - Solution ---
  *
  *   - Part I: 42951
  *   - Part II: 18568
  */
object Day8:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_8_input.txt")
    val root = readAllLines(inputStream)(parseData)
      .map(mkTree).getOrElse(throw new IllegalArgumentException)

    val sumOfMetadataEntries = part1(root)
    println(s"Part I: the sum of all metadata entries is $sumOfMetadataEntries.")

    val rootValue = part2(root)
    println(s"Part II: the value of the root node is $rootValue.")

  def part1(root: Node): Int =
    @tailrec
    def go(nodes: Vector[Node], totalSum: Int): Int = nodes match
      case Vector() => totalSum
      case ns       => go(ns.flatMap(_.childNodes), totalSum + ns.flatMap(_.metadata).sum)

    go(Vector(root), totalSum = 0)

  def part2(root: Node): Int =
    @tailrec
    def go(nodes: Vector[Node], totalValue: Int): Int = nodes match
      case Vector() => totalValue
      case ns =>
        val (parentNodes, leafNodes) = ns.partition(_.childNodes.nonEmpty)

        val value = leafNodes.flatMap(_.metadata).sum
        val nextNodes = parentNodes.flatMap { parentNode =>
          val childNodes = parentNode.childNodes

          parentNode.metadata.map(i => i - 1)
            .filter(childNodes.isDefinedAt)
            .map(childIndex => childNodes(childIndex))
        }

        go(nextNodes, totalValue + value)

    go(Vector(root), totalValue = 0)

  def mkTree(xs: Vector[Int]): Node =
    def go(treeStructure: Vector[Int]): (Node, Vector[Int]) = treeStructure match
      case childNodeCount +: metadataEntryCount +: rest =>
        val (childNodes, remainingTreeStructure) = (0 until childNodeCount).foldLeft((Vector.empty[Node], rest)) {
          case ((nodes, ys), _) =>
            val (childNode, remaining) = go(ys)

            (nodes :+ childNode, remaining)
        }

        val (metadata, ts) = remainingTreeStructure.splitAt(metadataEntryCount)

        Node(metadata, childNodes) -> ts
      case _ => throw new IllegalArgumentException()

    go(xs) match
      case (root, _) => root

  def parseData(data: Iterator[String]): Vector[Int] =
    data.flatMap(data => data.split(" ").map(_.toInt)).toVector

  case class Node(metadata: Vector[Int], childNodes: Vector[Node])
