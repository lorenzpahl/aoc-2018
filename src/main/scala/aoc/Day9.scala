/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

/** --- Day 9: Marble Mania - Solution ---
  *
  * Input: 423 players; last marble is worth 71944 points. For part II the number of the last marble is 100 times
  * larger.
  *
  *   - Part I: 418237
  *   - Part II: 3505711612
  */
object Day9:
  val numberOfPlayers: Int = 423
  val scoreOfLastMarble: Long = 71_944
  val scoreOfMagicMarble: Int = 23

  def run(): Unit = playGame().foreach {
    (winner, score) => println(s"The winning Elf's score is ${score} and the player number is ${winner}.")
  }

  def playGame(): Option[(Int, Long)] =
    def players(): LazyList[Int] = LazyList.iterate(1) { i => if i < numberOfPlayers then i + 1 else 1 }

    val playersWithMarble = players().zip(1L to scoreOfLastMarble)
    val (_, finalScores) = playersWithMarble.foldLeft((MarbleNode(0L), Map.empty[Int, Long])) {
      case ((currentMarble, scoresByPlayer), (player, nextMarble)) =>
        if isMultipleOfMagicMarble(nextMarble) then
          // Remove the marble that is 7 marbles counter-clockwise before the current marble.
          val (marble, newCurrentMarble) = currentMarble.removeAt(-7)

          val currentPlayerScore = scoresByPlayer.getOrElse(player, 0L)
          val updatedScoresByPlayer = scoresByPlayer.updated(player, currentPlayerScore + marble.value + nextMarble)

          (newCurrentMarble, updatedScoresByPlayer)
        else
          // Insert the next marble between the marbles that are 1 and 2 marbles clockwise of the current marble.
          val newCurrentMarble = currentMarble.insertAt(2, nextMarble)

          (newCurrentMarble, scoresByPlayer)
    }

    // Select the player with the highest score.
    finalScores.maxByOption { (_, score) => score }

  def isMultipleOfMagicMarble(marble: Long): Boolean = marble % scoreOfMagicMarble == 0L

  class MarbleNode private (val value: Long, var prev: MarbleNode, var next: MarbleNode):
    def nthPredecessor(n: Long): MarbleNode =
      (0L until n).foldLeft(this) { (node, _) => node.prev }

    def nthSuccessor(n: Long): MarbleNode =
      (0L until n).foldLeft(this) { (node, _) => node.next }

    def removeAt(i: Long): (MarbleNode, MarbleNode) =
      val node = if i < 0L then this.nthPredecessor(Math.abs(i)) else this.nthSuccessor(i)
      val prev = node.prev
      val next = node.next

      node.prev.next = next
      node.next.prev = prev

      node.prev = node
      node.next = node

      (node, next)

    def insertAt(i: Long, value: Long): MarbleNode =
      if i < 0L then
        val insertPos = this.nthPredecessor(Math.abs(i))
        val node = new MarbleNode(value, insertPos, insertPos.next)

        insertPos.next.prev = node
        insertPos.next = node

        node
      else
        val insertPos = this.nthSuccessor(i)
        val node = new MarbleNode(value, insertPos.prev, insertPos)

        insertPos.prev.next = node
        insertPos.prev = node

        node

  object MarbleNode:
    def apply(value: Long): MarbleNode =
      val node = new MarbleNode(value, prev = null, next = null)
      node.prev = node
      node.next = node

      node
