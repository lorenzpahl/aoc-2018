/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec

/** --- Day 12: Subterranean Sustainability - Solution ---
  *
  *   - Part I: 2917
  *   - Part II: 3250000000956
  */
object Day12:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_12_input.txt")
    val (initialState, spreadNotes) = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val plantNumberSumA = simulatePlantCycle(initialState, spreadNotes, cycleCount = 20L)
    println(
      s"Part I: the sum of the numbers of all pots which contain a plant after 20 generations is ${plantNumberSumA}."
    )

    val plantNumberSumB = simulatePlantCycle(initialState, spreadNotes, cycleCount = 50_000_000_000L)
    println(
      s"Part II: the sum of the numbers of all pots which contain a plant after 50.000.000.000 generations is ${plantNumberSumB}."
    )

  def simulatePlantCycle(initialState: IndexedSeq[Pot], spreadNotes: Map[SpreadNote, Char], cycleCount: Long): Long =
    @tailrec
    def go(currentGen: IndexedSeq[Pot], counter: Long): (Long, IndexedSeq[Pot], IndexedSeq[Pot]) =
      if counter == cycleCount then (counter, currentGen, currentGen)
      else
        val nextGen = slidingCollect(expandWithEmptyPots(currentGen))
          .map { (centerPot, spreadNote) => Pot(centerPot.id, spreadNotes(spreadNote)) }
          .toIndexedSeq

        val isSameState = currentGen.iterator.zip(nextGen.iterator)
          .forall { case (Pot(_, prevContent), Pot(_, nextContent)) => prevContent == nextContent }

        if isSameState then (counter, currentGen, nextGen)
        else go(nextGen, counter + 1)

    def nonEmptyPotNumberSum(pots: IndexedSeq[Pot]): Int =
      pots.iterator.collect({ case pot if pot.nonEmpty => pot.id }).sum

    val (generation, previousGeneration, currentGeneration) = go(initialState, counter = 0)
    val s1 = nonEmptyPotNumberSum(previousGeneration)
    val s2 = nonEmptyPotNumberSum(currentGeneration)
    val diff = Math.abs(s1 - s2)

    (cycleCount - generation) * diff + s1

  def countWhile[A](seq: IndexedSeq[A], fromStart: Boolean)(p: A => Boolean): Int =
    @tailrec
    def go(currentIndex: Int, incrementValue: Int, count: Int): Int =
      if seq.isDefinedAt(currentIndex) && p(seq(currentIndex)) then
        go(currentIndex + incrementValue, incrementValue, count + 1)
      else count

    val (startIndex, incrementValue) = if fromStart then 0 -> 1 else (seq.size - 1) -> -1
    go(startIndex, incrementValue, count = 0)

  def expandWithEmptyPots(pots: IndexedSeq[Pot]): IndexedSeq[Pot] =
    def expandStart(ps: IndexedSeq[Pot]): IndexedSeq[Pot] =
      val startPotId = ps.head.id
      val emptyPotsCount = countWhile(ps, fromStart = true)(_.isEmpty)

      val expandCount = Math.max(0, 4 - emptyPotsCount)
      val emptyPots = IndexedSeq.iterate(startPotId - expandCount, expandCount)(_ + 1).map(id => Pot(id, '.'))

      emptyPots ++ ps

    def expandEnd(ps: IndexedSeq[Pot]): IndexedSeq[Pot] =
      val lastPotId = ps.last.id
      val emptyPotsCount = countWhile(ps, fromStart = false)(_.isEmpty)

      val expandCount = Math.max(0, 4 - emptyPotsCount)
      val emptyPots = IndexedSeq.iterate(lastPotId + 1, expandCount)(_ + 1).map(id => Pot(id, '.'))

      ps ++ emptyPots

    expandStart(expandEnd(pots))

  def slidingCollect(seq: Seq[Pot]): Iterator[(Pot, SpreadNote)] =
    seq.sliding(5).map(SpreadNote.from)

  def parseData(lines: Iterator[String]): (IndexedSeq[Pot], Map[SpreadNote, Char]) =
    val initialStatePattern =
      """initial state: ([#.]+)""".r

    val spreadPattern =
      """([#.]{5}) => ([#.])""".r

    val (initialStateIterator, remainingLinesIterator) = lines.span(initialStatePattern.matches(_))

    val initialState = initialStateIterator.collectFirst {
      case initialStatePattern(pots) => pots.toCharArray.zipWithIndex
          .map { (content, idx) => Pot(idx, content) }.toVector
    }.getOrElse(throw new IllegalArgumentException("Expected to find the initial state of the pots"))

    val spreadNotes = remainingLinesIterator.filter(_.nonEmpty).map({
      case spreadPattern(currentState, nextState) =>
        SpreadNote(currentState(0), currentState(1), currentState(2), currentState(3), currentState(4)) -> nextState(0)
      case line => throw new IllegalArgumentException(s"Could not parse ${line}")
    }).toMap

    (initialState, spreadNotes)

  case class Pot(id: Int, content: Char):
    def isEmpty: Boolean = content == '.'

    def nonEmpty: Boolean = !this.isEmpty

  case class SpreadNote(outerLeft: Char, left: Char, center: Char, right: Char, outerRight: Char)

  object SpreadNote:
    def from(pots: Seq[Pot]): (Pot, SpreadNote) =
      pots match
        case Seq(ol: Pot, l: Pot, c: Pot, r: Pot, or: Pot) =>
          c -> SpreadNote(ol.content, l.content, c.content, r.content, or.content)
        case _ => throw new IllegalArgumentException
