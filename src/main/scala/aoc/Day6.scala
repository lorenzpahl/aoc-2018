/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec
import scala.language.implicitConversions

/** --- Day 6: Chronal Coordinates - Solution ---
  *
  *   - Part I: 3620
  *   - Part II: 39930
  */
object Day6:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_6_input.txt")
    val origins = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val rivals = origins.to(LazyList)
    val finite = origins.filterNot(origin => canGrowInfinite(origin, rivals.filter(rival => rival != origin)))
    val occupiedAreas = areas(finite, origins)

    // Part I
    val largestArea = occupiedAreas.maxBy(_.size)
    println(s"Part I: the size of the largest finite area is ${largestArea.size}.")

    // Part II
    val safeRegion = region(origins, _ < 10_000)
    println(s"Part II: the size of the specified region is ${safeRegion.size}.")

  def region(origins: Set[Coordinate], includeIf: Int => Boolean): Set[Coordinate] =
    def totalDistanceTo(coordinate: Coordinate): Int =
      origins.foldLeft(0) { (distance, p) => distance + p.distanceTo(coordinate) }

    origins.filter(origin => includeIf(totalDistanceTo(origin)))
      .flatMap { origin =>
        val occupiedArea = occupyArea(origin, (_, claimedCoordinate) => includeIf(totalDistanceTo(claimedCoordinate)))
        occupiedArea.occupiedCoordinates
      }

  def areas(origins: Set[Coordinate], rivals: Set[Coordinate]): Set[Area] =
    origins.map { origin =>
      occupyArea(
        origin,
        (claimer, claimedCoordinate) => rivals.forall(rival => canClaim(claimer, rival, claimedCoordinate))
      )
    }

  def occupyArea(origin: Coordinate, claimIf: (Coordinate, Coordinate) => Boolean): Area =
    @tailrec
    def claimYAxes(
        claimer: Coordinate,
        posY: Int,
        direction: Direction,
        claimedCoordinates: Set[Coordinate]
    ): Set[Coordinate] =
      val claimedCoordinate = Coordinate(claimer.x, posY)
      val nextPosY = if direction == Direction.Top then posY - 1 else posY + 1

      if claimIf(claimer, claimedCoordinate) then
        claimYAxes(claimer, nextPosY, direction, claimedCoordinates + claimedCoordinate)
      else claimedCoordinates

    @tailrec
    def claimXAxes(
        claimer: Coordinate,
        posX: Int,
        posY: Int,
        direction: Direction,
        claimedCoordinates: Set[Coordinate]
    ): Set[Coordinate] =
      val claimedCoordinate = Coordinate(posX, posY)
      val nextPosX = if direction == Direction.Left then posX - 1 else posX + 1

      if claimIf(claimer, claimedCoordinate) then
        claimXAxes(claimer, nextPosX, posY, direction, claimedCoordinates + claimedCoordinate)
      else claimedCoordinates

    val vs = claimYAxes(origin, origin.y - 1, Direction.Top, Set())
    val ws = claimYAxes(origin, origin.y + 1, Direction.Down, Set())

    val ys = vs ++ Set(origin) ++ ws
    val claimedCoordinates = ys.flatMap(claimed => {
      val hs = claimXAxes(origin, origin.x - 1, claimed.y, Direction.Left, Set())
      val is = claimXAxes(origin, origin.x + 1, claimed.y, Direction.Right, Set())

      hs ++ Set(claimed) ++ is
    })

    new Area(origin).expand(claimedCoordinates)

  def canGrowInfinite(claimer: Coordinate, rivals: LazyList[Coordinate]): Boolean =
    val intersections = rivals.map(rival => {
      val (verticalIntersection, horizontalIntersection) = claimer.intersectionWith(rival)

      ((rival, verticalIntersection), (rival, horizontalIntersection))
    })

    val (verticalIntersections, horizontalIntersections) = intersections.unzip
    val (vs, ws) = verticalIntersections.partition { (_, p) => p.y < claimer.y }
    val (hs, is) = horizontalIntersections.partition { (_, p) => p.x < claimer.x }

    vs.forall { (rival, p) => canClaim(claimer, rival, p) } ||
    ws.forall { (rival, p) => canClaim(claimer, rival, p) } ||
    hs.forall { (rival, p) => canClaim(claimer, rival, p) } ||
    is.forall { (rival, p) => canClaim(claimer, rival, p) }

  def canClaim(claimer: Coordinate, rival: Coordinate, claimedCoordinate: Coordinate): Boolean =
    val d1 = claimer.distanceTo(claimedCoordinate)
    val d2 = rival.distanceTo(claimedCoordinate)

    (claimer == rival) || (d1 < d2)

  def parseData(data: Iterator[String]): Set[Coordinate] =
    val coordinatePattern =
      """(\d+), (\d+)""".r

    data.map {
      case coordinatePattern(x, y) => Coordinate(x.toInt, y.toInt)
      case line                    => throw new IllegalArgumentException(s"Could not parse '$line'")
    }.toSet

  enum Direction:
    case Top
    case Right
    case Down
    case Left

  case class Coordinate(x: Int, y: Int):

    /** Calculates the Manhattan distance between `this` and `other`.
      *
      * @param other
      *   used to calculate the distance to this coordinate.
      * @return
      *   distance from `this` to `other`.
      */
    def distanceTo(other: Coordinate): Int =
      Math.abs(x - other.x) + Math.abs(y - other.y)

    def intersectionWith(other: Coordinate): (Coordinate, Coordinate) =
      (Coordinate(x, other.y), Coordinate(other.x, y))

  class Area private (val origin: Coordinate, private val occupiedArea: Set[Coordinate]):
    def this(origin: Coordinate) =
      this(origin, Set(origin))

    def occupiedCoordinates: Set[Coordinate] = occupiedArea

    def expand(coordinates: Set[Coordinate]): Area = new Area(origin, occupiedArea ++ coordinates)

    def occupies(coordinate: Coordinate): Boolean = origin == coordinate || occupiedArea.contains(coordinate)

    def size: Int = occupiedArea.size

    def canEqual(other: Any): Boolean = other.isInstanceOf[Area]

    override def equals(other: Any): Boolean =
      other match
        case that: Area => that.canEqual(this) &&
            origin == that.origin &&
            occupiedArea == that.occupiedArea
        case _ => false

    override def hashCode(): Int =
      val state = Seq(origin, occupiedArea)
      state.map(_.hashCode()).foldLeft(0) { (a, b) => 31 * a + b }
