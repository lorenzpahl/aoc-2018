/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines
import aoc.extension.IteratorOps.window

import scala.annotation.tailrec

/** --- Day 16: Chronal Classification - Solution ---
  *
  *   - Part I: 547
  *   - Part II: 582
  */
object Day16:
  def run(): Unit =
    val opcodes = Opcode.values
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_16_input.txt")
    val (matchingSamplesByOpcodeId, matchingOpcodesById, testProgram) = readAllLines(inputStream) {
      lines =>
        parseData(lines) match
          case (samples, testData) =>
            val samplesByOpcodeId = samples.map {
              case Sample(input, output, opcodeId, a, b, c) =>
                val matchingOpcodes = opcodes.view
                  .map(opcode => Instruction(opcode, a, b, c))
                  .map(instruction => instruction.opcode -> input.process(instruction))
                  .filter { (_, result) => result == output }
                  .toArray

                opcodeId -> matchingOpcodes
            }.toSeq

            val opcodesById = samplesByOpcodeId.groupMapReduce { (opcodeId, _) => opcodeId } {
              (_, matchingOpcodes) => matchingOpcodes.map { (opcode, _) => opcode }.toSet
            } {
              _ ++ _
            }

            (samplesByOpcodeId, opcodesById, testData.toSeq)
    }.getOrElse(throw new IllegalArgumentException)

    val matchingSampleCount = matchingSamplesByOpcodeId.count {
      (_, matchingOpcodes) => matchingOpcodes.length >= 3
    }

    println(s"Part I: ${matchingSampleCount} samples behave like three or more opcodes.")

    val uniqueOpcodesById = mkOpcodeMap(matchingOpcodesById)(_.size == opcodes.length)
    val initialState = Device(Register(0), Register(0), Register(0), Register(0))
    val finalState = testProgram.map {
      (opcodeId, a, b, c) => Instruction(uniqueOpcodesById(opcodeId), a, b, c)
    }.foldLeft(initialState)((state, instruction) => state.process(instruction))

    println(s"Part II: Register 0 contains the value ${finalState(0)}.")

  def mkOpcodeMap(opcodesByIdentifier: Map[OpcodeId, Set[Opcode]])(terminateWhen: Map[OpcodeId, Opcode] => Boolean)
      : Map[OpcodeId, Opcode] =
    @tailrec
    def go(opcodesById: Map[OpcodeId, Set[Opcode]], uniqueOpcodesById: Map[OpcodeId, Opcode]): Map[OpcodeId, Opcode] =
      if terminateWhen(uniqueOpcodesById) || opcodesById.isEmpty then uniqueOpcodesById
      else
        val newUniqueOpcodesById = opcodesById.collect({
          case (opcodeId, opcodes) if opcodes.size == 1 => opcodeId -> opcodes.head
        })

        val newUniqueOpcodes = newUniqueOpcodesById.values.toSet
        val updatedUniqueOpcodesById = uniqueOpcodesById ++ newUniqueOpcodesById
        val updatedOpcodesById = opcodesById.keys.foldLeft(opcodesById) { (intermediateOpcodesById, opcodeId) =>
          intermediateOpcodesById.updatedWith(opcodeId) {
            case Some(_) if newUniqueOpcodesById.contains(opcodeId) => None
            case Some(opcodes) => opcodes.removedAll(newUniqueOpcodes) match
                case updatedOpcodes if updatedOpcodes.isEmpty => None
                case updatedOpcodes                           => Some(updatedOpcodes)
            case _ => throw new IllegalStateException()
          }
        }

        go(updatedOpcodesById, updatedUniqueOpcodesById)

    go(opcodesByIdentifier, Map.empty)

  def parseData(lines: Iterator[String]): (Iterator[Sample], Iterator[(OpcodeId, Int, Int, Int)]) =
    val registersPattern =
      """.+ \[(\d), (\d), (\d), (\d)]""".r

    def parseDevice(rawDevice: String): Device = rawDevice match
      case registersPattern(i1, i2, i3, i4) =>
        Device(Register(i1.toInt), Register(i2.toInt), Register(i3.toInt), Register(i4.toInt))
      case _ => throw new IllegalStateException()

    def parseInstruction(rawInstruction: String): (OpcodeId, Int, Int, Int) =
      rawInstruction.split(" ") match
        case Array(opcodeId, a, b, c) => (opcodeId.toShort, a.toInt, b.toInt, c.toInt)
        case _                        => throw new IllegalStateException()

    val (rawSamples, rawTestData) = lines.window(_.nonEmpty, _.isEmpty).partition(_.size == 3)
    val samples = rawSamples.map({
      case Seq(rawInput, instruction, rawOutput) =>
        val input = parseDevice(rawInput)
        val output = parseDevice(rawOutput)
        parseInstruction(instruction) match {
          case (opcodeId, a, b, c) => Sample(input, output, opcodeId, a, b, c)
        }
      case _ => throw new IllegalStateException()
    })

    val testData = rawTestData.flatten.map(parseInstruction)

    samples -> testData

  type OpcodeId = Short
  type Address = Int

  final case class Sample(input: Device, output: Device, opcodeId: OpcodeId, a: Int, b: Int, c: Int)

  enum Opcode:
    case Addr
    case Addi
    case Mulr
    case Muli
    case Banr
    case Bani
    case Borr
    case Bori
    case Setr
    case Seti
    case Gtir
    case Gtri
    case Gtrr
    case Eqir
    case Eqri
    case Eqrr

  final case class Instruction(opcode: Opcode, a: Int, b: Int, c: Int)

  final case class Register(value: Int)

  final case class Device(register1: Register, register2: Register, register3: Register, register4: Register):

    def apply(address: Address): Int = address match
      case 0 => register1.value
      case 1 => register2.value
      case 2 => register3.value
      case 3 => register4.value
      case _ => throw new IllegalArgumentException()

    def updated(address: Address, newValue: Int): Device = address match
      case 0 => Device(register1.copy(value = newValue), register2, register3, register4)
      case 1 => Device(register1, register2.copy(value = newValue), register3, register4)
      case 2 => Device(register1, register2, register3.copy(value = newValue), register4)
      case 3 => Device(register1, register2, register3, register4.copy(value = newValue))
      case _ => throw new IllegalArgumentException()

    def process(instruction: Instruction): Device = instruction match
      case Instruction(Opcode.Addr, ra, rb, rc) => updated(rc, apply(ra) + apply(rb))
      case Instruction(Opcode.Addi, ra, ia, rc) => updated(rc, apply(ra) + ia)
      case Instruction(Opcode.Mulr, ra, rb, rc) => updated(rc, apply(ra) * apply(rb))
      case Instruction(Opcode.Muli, ra, ib, rc) => updated(rc, apply(ra) * ib)
      case Instruction(Opcode.Banr, ra, rb, rc) => updated(rc, apply(ra) & apply(rb))
      case Instruction(Opcode.Bani, ra, ib, rc) => updated(rc, apply(ra) & ib)
      case Instruction(Opcode.Borr, ra, rb, rc) => updated(rc, apply(ra) | apply(rb))
      case Instruction(Opcode.Bori, ra, ib, rc) => updated(rc, apply(ra) | ib)
      case Instruction(Opcode.Setr, ra, _, rc)  => updated(rc, apply(ra))
      case Instruction(Opcode.Seti, ia, _, rc)  => updated(rc, ia)
      case Instruction(Opcode.Gtir, ia, rb, rc) => updated(rc, if ia > apply(rb) then 1 else 0)
      case Instruction(Opcode.Gtri, ra, ib, rc) => updated(rc, if apply(ra) > ib then 1 else 0)
      case Instruction(Opcode.Gtrr, ra, rb, rc) => updated(rc, if apply(ra) > apply(rb) then 1 else 0)
      case Instruction(Opcode.Eqir, ia, rb, rc) => updated(rc, if ia == apply(rb) then 1 else 0)
      case Instruction(Opcode.Eqri, ra, ib, rc) => updated(rc, if apply(ra) == ib then 1 else 0)
      case Instruction(Opcode.Eqrr, ra, rb, rc) => updated(rc, if apply(ra) == apply(rb) then 1 else 0)
