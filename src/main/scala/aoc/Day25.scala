/*
 * Copyright 2021 Lorenz Pahl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aoc

import aoc.IOUtils.readAllLines

import scala.annotation.tailrec

/** --- Day 25: Four-Dimensional Adventure ---
  *
  *   - Part I: 305
  */
object Day25:
  def run(): Unit =
    val inputStream = getClass.getResourceAsStream("/aoc/AoC_2018_day_25_input.txt")
    val fixedPoints = readAllLines(inputStream)(parseData)
      .getOrElse(throw new IllegalArgumentException)

    val constellations = findConstellations(fixedPoints)
    println(s"${constellations.size} constellations are formed by the fixed points in spacetime.")

  def findConstellations(fixedPoints: Seq[Points.Point4D]): Set[Constellation] =
    @tailrec
    def buildChain(
        ps: Seq[Points.Point4D],
        visited: Set[Points.Point4D],
        constellation: Constellation = Set()
    ): (Constellation, Set[Points.Point4D]) =
      ps match
        case Seq()                            => constellation -> visited
        case p +: rest if visited.contains(p) => buildChain(rest, visited, constellation)
        case p +: rest =>
          val updatedVisited = visited + p
          val updatedConstellation = constellation + p
          val candidates = rest ++ fixedPoints.view.filterNot(visited.contains)
            .filter(q => q.distanceTo(p) <= 3)

          buildChain(candidates, updatedVisited, updatedConstellation)

    fixedPoints.foldLeft((Set.empty[Constellation], Set.empty[Points.Point4D])) {
      case ((constellations, visited), p) =>
        val (newConstellation, updatedVisited) = buildChain(Seq(p), visited)
        if newConstellation.isEmpty then constellations -> updatedVisited
        else (constellations + newConstellation) -> updatedVisited
    } match
      case (constellations, _) => constellations

  def parseData(lines: Iterator[String]): Seq[Points.Point4D] =
    lines.map(_.trim)
      .map { line =>
        line.split(",").map(_.trim) match
          case Array(a, b, c, d) => Points.Point4D(a.toInt, b.toInt, c.toInt, d.toInt)
          case _                 => throw new IllegalArgumentException
      }.toSeq

  type Constellation = Set[Points.Point4D]

  object Points:
    opaque type Point4D = (Int, Int, Int, Int)

    object Point4D:
      def apply(a: Int, b: Int, c: Int, d: Int): Point4D = (a, b, c, d)

    extension (p: Point4D)
      def distanceTo(q: Point4D): Int = (p, q) match
        case ((a1, b1, c1, d1), (a2, b2, c2, d2)) =>
          Math.abs(a1 - a2) + Math.abs(b1 - b2) + Math.abs(c1 - c2) + Math.abs(d1 - d2)
