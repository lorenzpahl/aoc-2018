/*
 * Copyright 2021 Lorenz Pahl
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package aoc.graph

/** Represents a directed graph.
  *
  * @param graph
  *   adjacency-list representation of the graph
  * @tparam VA
  *   type of the vertex attribute
  * @tparam EA
  *   type of the edge attribute
  */
final class Digraph[VA, EA] private (graph: Map[Int, (Vertex[VA], Set[Edge[EA]])]):
  def vertices(): Iterable[Vertex[VA]] = graph.values.map { (v, _) => v }

  def edges(): Iterable[Edge[EA]] = graph.values.flatMap { (_, es) => es }

  def edges(vertexId: Int | Vertex[VA]): Set[Edge[EA]] =
    val key =
      vertexId match
        case id: Int       => id
        case Vertex(id, _) => id

    graph.getOrElse(key, throw new IndexOutOfBoundsException()) match
      case (_, edges) => edges

  def adj(vertexId: Int | Vertex[VA]): Iterable[Vertex[VA]] =
    val key =
      vertexId match
        case id: Int       => id
        case Vertex(id, _) => id

    graph.getOrElse(key, throw new IndexOutOfBoundsException()) match
      case (_, edges) => edges.foldLeft(Set.empty[Vertex[VA]]) { (vs, edge) =>
          val vertex = graph.get(edge.destId).map { (v, _) => v }

          vs ++ vertex
        }

  def reverse(): Digraph[VA, EA] =
    val vertices = this.vertices()
    val edges = this.edges().map { case Edge(srcId, destId, attr) => Edge(destId, srcId, attr) }

    Digraph(vertices.toSet, edges.toSet)

object Digraph:
  def apply[VA, EA](vertices: Set[Vertex[VA]], edges: Set[Edge[EA]]): Digraph[VA, EA] =
    val graphRepresentation = vertices.map(vertex => (vertex.id, (vertex, Set.empty[Edge[EA]]))).toMap
    val graph = edges.foldRight(graphRepresentation) { (edge, map) =>
      map.updatedWith(edge.srcId) {
        case Some((v, es)) => Some((v, es + edge))
        case _             => None
      }
    }

    new Digraph(graph)
