/*
 * Copyright 2021 Lorenz Pahl
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package aoc.graph

/** Represents an undirected graph.
  *
  * @param graph
  *   adjacency-list representation of the graph
  * @tparam VA
  *   type of the vertex attribute
  * @tparam EA
  *   type of the edge attribute
  */
final class Graph[VA, EA] private (graph: Map[Int, (Vertex[VA], Set[Edge[EA]])])
    extends PartialFunction[Int, Vertex[VA]]:
  def vertices(): Iterable[Vertex[VA]] = graph.values.map { (v, _) => v }

  def edges(): Iterable[Edge[EA]] = graph.values.flatMap { (_, es) => es }

  def adj(vertexId: Int): Iterable[Vertex[VA]] =
    graph.getOrElse(vertexId, throw new IndexOutOfBoundsException()) match
      case (_, edges) => edges.foldLeft(Set.empty[Vertex[VA]]) { (vs, edge) =>
          val vertex = graph.get(edge.destId).map { (v, _) => v }

          vs ++ vertex
        }

  override def isDefinedAt(vertexId: Int): Boolean = graph.isDefinedAt(vertexId)

  override def apply(vertexId: Int): Vertex[VA] = graph.get(vertexId).map { (v, _) => v }
    .getOrElse(throw new NoSuchElementException())

object Graph:
  def apply[VA, EA](vertices: Set[Vertex[VA]], edges: Set[Edge[EA]]): Graph[VA, EA] =
    val graphRepresentation = vertices.map(vertex => (vertex.id, vertex -> Set.empty[Edge[EA]])).toMap
    val graph = edges.foldRight(graphRepresentation) { (edge, map) =>
      map.updatedWith(edge.srcId) {
        case Some((v, es)) => Some((v, es + edge))
        case _             => None
      }.updatedWith(edge.destId) {
        case Some((v, es)) => Some((v, es + Edge(edge.destId, edge.srcId, edge.attr)))
        case _             => None
      }
    }

    new Graph[VA, EA](graph)
