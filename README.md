# Advent of Code 2018 - Solutions

https://adventofcode.com/2018

## Puzzles

| Day  | Done  | Puzzle |
| :--- | :---: | :----: |
| 1  | ✔ | [Chronal Calibration](https://adventofcode.com/2018/day/1) \[[solution](src/main/scala/aoc/Day1.scala)\]                        |
| 2  | ✔ | [Inventory Management System](https://adventofcode.com/2018/day/2) \[[solution](src/main/scala/aoc/Day2.scala)\]                |
| 3  | ✔ | [No Matter How You Slice It](https://adventofcode.com/2018/day/3) \[[solution](src/main/scala/aoc/Day3.scala)\]                 |
| 4  | ✔ | [Repose Record](https://adventofcode.com/2018/day/4) \[[solution](src/main/scala/aoc/Day4.scala)\]                              |
| 5  | ✔ | [Alchemical Reduction](https://adventofcode.com/2018/day/5) \[[solution](src/main/scala/aoc/Day5.scala)\]                       |
| 6  | ✔ | [Chronal Coordinates](https://adventofcode.com/2018/day/6) \[[solution](src/main/scala/aoc/Day6.scala)\]                        |
| 7  | ✔ | [The Sum of Its Parts](https://adventofcode.com/2018/day/7) \[[solution](src/main/scala/aoc/Day7.scala)\]                       |
| 8  | ✔ | [Memory Maneuver](https://adventofcode.com/2018/day/8) \[[solution](src/main/scala/aoc/Day8.scala)\]                            |
| 9  | ✔ | [Marble Mania](https://adventofcode.com/2018/day/9) \[[solution](src/main/scala/aoc/Day9.scala)\]                               |
| 10 | ✔ | [The Stars Align](https://adventofcode.com/2018/day/10) \[[solution](src/main/scala/aoc/Day10.scala)\]                          |
| 11 | ✔ | [Chronal Charge](https://adventofcode.com/2018/day/11) \[[solution](src/main/scala/aoc/Day11.scala)\]                           |
| 12 | ✔ | [Subterranean Sustainability](https://adventofcode.com/2018/day/12) \[[solution](src/main/scala/aoc/Day12.scala)\]              |
| 13 | ✔ | [Mine Cart Madness](https://adventofcode.com/2018/day/13) \[[solution](src/main/scala/aoc/Day13.scala)\]                        |
| 14 | ✔ | [Chocolate Charts](https://adventofcode.com/2018/day/14) \[[solution](src/main/scala/aoc/Day14.scala)\]                         |
| 15 | ✔ | [Beverage Bandits](https://adventofcode.com/2018/day/15) \[[solution](src/main/scala/aoc/Day15.scala)\]                         |
| 16 | ✔ | [Chronal Classification](https://adventofcode.com/2018/day/16) \[[solution](src/main/scala/aoc/Day16.scala)\]                   |
| 17 | ✔ | [Reservoir Research](https://adventofcode.com/2018/day/17) \[[solution](src/main/scala/aoc/Day17.scala)\]                       |
| 18 | ✔ | [Settlers of The North Pole](https://adventofcode.com/2018/day/18) \[[solution](src/main/scala/aoc/Day18.scala)\]               |
| 19 | ✔ | [Go With The Flow](https://adventofcode.com/2018/day/19) \[[solution](src/main/scala/aoc/Day19.scala)\]                         |
| 20 | ✔ | [A Regular Map](https://adventofcode.com/2018/day/20) \[[solution](src/main/scala/aoc/Day20.scala)\]                            |
| 21 | ✔ | [Chronal Conversion](https://adventofcode.com/2018/day/21) \[[solution](src/main/scala/aoc/Day21.scala)\]                       |
| 22 | ✔ | [Mode Maze](https://adventofcode.com/2018/day/22) \[[solution](src/main/scala/aoc/Day22.scala)\]                                |
| 23 | ✔ | [Experimental Emergency Teleportation](https://adventofcode.com/2018/day/23) \[[solution](src/main/scala/aoc/Day23.scala)\]     |
| 24 | ✔ | [Immune System Simulator 20XX](https://adventofcode.com/2018/day/24) \[[solution](src/main/scala/aoc/Day24.scala)\]             |
| 25 | ✔ | [Four-Dimensional Adventure](https://adventofcode.com/2018/day/25) \[[solution](src/main/scala/aoc/Day25.scala)\]               |

## Build and Run

Run one of the solutions:

```bash
$ git clone https://gitlab.com/lorenzpahl/aoc-2018.git
$ cd ./aoc-2018
$ sbt "run <puzzle>" # e.g. sbt "run day5"
```

Package as a universal `zip` file using [sbt native packager](https://github.com/sbt/sbt-native-packager):

```bash
$ sbt universal:packageBin
```
